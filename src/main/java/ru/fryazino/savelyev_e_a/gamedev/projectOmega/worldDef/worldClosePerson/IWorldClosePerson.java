package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldClosePerson;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;

@FunctionalInterface
public interface IWorldClosePerson<WD extends WorldData> {
	void closePerson(WorldClosePersonData<WD> data);
}