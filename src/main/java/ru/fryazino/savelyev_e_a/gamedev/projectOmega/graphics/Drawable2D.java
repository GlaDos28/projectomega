package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.IPoint;

@FunctionalInterface
public interface Drawable2D {
	void draw(GraphicsAdapter graphics, Camera2D camera, IPoint offset, Object... args);

	default void draw(GraphicsAdapter graphics, Camera2D camera, Object... args) {
		this.draw(graphics, camera, new IPoint(0, 0), args);
	}

	default void draw(GraphicsAdapter graphics, IPoint offset, Object... args) {
		this.draw(graphics, new Camera2D(new IPoint(0, 0)), offset, args);
	}

	default void draw(GraphicsAdapter graphics, Object... args) {
		this.draw(graphics, new Camera2D(new IPoint(0, 0)), new IPoint(0, 0), args);
	}

	default void draw(GraphicsAdapter graphics, Camera2D camera) {
		this.draw(graphics, camera, new IPoint(0, 0));
	}

	default void draw(GraphicsAdapter graphics, IPoint offset) {
		this.draw(graphics, new Camera2D(new IPoint(0, 0)), offset);
	}

	default void draw(GraphicsAdapter graphics) {
		this.draw(graphics, new Camera2D(new IPoint(0, 0)), new IPoint(0, 0));
	}
}