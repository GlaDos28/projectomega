package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldCloseWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;

@FunctionalInterface
public interface IWorldCloseWorld<WD extends WorldData> {
	void closeWorld(WorldCloseWorldData<WD> data);
}