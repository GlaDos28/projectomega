package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.IPoint;

import java.awt.*;
import java.awt.Image;

public final class GraphicsAdapter {
	private final Graphics graphics;
	private final GraphicsConfig config;

	public GraphicsAdapter(Graphics graphics, GraphicsConfig config) {
		this.graphics = graphics;
		this.config   = config;
	}

	//**

	public Graphics getGraphics() {
		return this.graphics;
	}

	public GraphicsConfig getConfig() {
		return this.config;
	}

	//**

	public void setColor(Color c) {
		this.graphics.setColor(c);
	}

	public void drawRect(int x, int y, int w, int h) {
		this.graphics.fillRect(x, this.config.getScrHeight() - y - h, w, h);
	}

	public void drawRect(double x, double y, double w, double h) {
		this.drawRect((int) x, (int) y, (int) w, (int) h);
	}

	public void drawRect(IPoint p, int w, int h) {
		this.drawRect(p.getX(), p.getY(), w, h);
	}

	public void drawRawImage(Image img, int x, int y, int w, int h, boolean flipX, boolean flipY) {
		this.graphics.drawImage(img, x + (flipX ? w : 0), this.config.getScrHeight() - y - h + (flipY ? h : 0), w * (flipX ? -1 : 1), h * (flipY ? -1 : 1), null);
	}

	public void drawRawImage(Image img, int x, int y, int w, int h) {
		this.drawRawImage(img, x, y, w, h, false, false);
	}

	public void drawRawImage(Image img, IPoint p, int w, int h) {
		this.drawRawImage(img, p.getX(), p.getY(), w, h);
	}

	public void drawRawImage(Image img, IPoint p, int w, int h, boolean flipX, boolean flipY) {
		this.drawRawImage(img, p.getX(), p.getY(), w, h, flipX, flipY);
	}

	public void drawImage(main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.elements.Image img, int x, int y) {
		this.drawRawImage(img.getRawImage(), x, y, img.getWidth(), img.getHeight());
	}

	public void drawImage(main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.elements.Image img, IPoint p) {
		this.drawRawImage(img.getRawImage(), p, img.getWidth(), img.getHeight());
	}

	//**

	@Override
	public String toString() {
		return "{ graphics: " + this.graphics.toString() + " }";
	}
}