package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldUpdatePerson;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.Person;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;

public final class WorldUpdatePersonData<WD extends WorldData> implements CSD<WorldUpdatePersonData<WD>> {
	private final Person person;

	public WorldUpdatePersonData(Person person) {
		this.person = person;
	}

	public Person getPerson() {
		return this.person;
	}

	@Override
	public WorldUpdatePersonData<WD> copy() {
		return new WorldUpdatePersonData<>(this.person.copy());
	}

	@Override
	public String toString() {
		return "{ person: " + this.person.toString() + " }";
	}
}