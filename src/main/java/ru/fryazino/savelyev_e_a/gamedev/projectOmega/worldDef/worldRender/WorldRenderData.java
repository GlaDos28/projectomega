package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldRender;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.Controller;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.game.GraphicsTransmitData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.Person;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;

public final class WorldRenderData<WD extends WorldData> {
	private final WD                   worldData;
	private final Person               person;
	private final GraphicsTransmitData graphicsData;
	private final Controller           controller;

	public WorldRenderData(WD worldData, Person person, GraphicsTransmitData graphicsData, Controller controller) {
		this.worldData    = worldData;
		this.person       = person;
		this.graphicsData = graphicsData;
		this.controller   = controller;
	}

	public WD getWorldData() {
		return this.worldData;
	}

	public <P extends Person> P getPerson() { /* careful with casting */
		return (P) this.person;
	}

	public GraphicsTransmitData getGraphicsData() {
		return this.graphicsData;
	}

	public Controller getController() {
		return this.controller;
	}

	@Override
	public String toString() {
		return "{ world-data: " + this.worldData.toString() + ", person: " + this.person.toString() + ", graphics-data: " + this.graphicsData.toString() + ", controller: " + this.controller.toString() + " }";
	}
}