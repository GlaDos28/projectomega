package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.game;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.GraphicsConfig;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.GameCycleIterationFunc;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.WorldDef;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldInitWorld.WorldInitWorldData;
import javafx.util.Pair;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.UtilFunctions;

import java.util.ArrayList;
import java.util.Collections;

public final class GameConfig implements CSD<GameConfig> {
	public static final int                    DEFAULT_SCREEN_WIDTH              = 1900;
	public static final int                    DEFAULT_SCREEN_HEIGHT             = 1000;
	public static final int                    DEFAULT_UR_MULT                   = 1;
	public static final long                   DEFAULT_MAX_UPDATE_TIME           = 100L;
	public static final GameCycleIterationType DEFAULT_GAME_CYCLE_ITERATION_TYPE = GameCycleIterationType.COMBINED;

	private ArrayList<WorldDef>                           worldDefs;
	private ArrayList<Pair<WorldDef, WorldInitWorldData>> initWorlds;
	private GraphicsConfig                                graphicsConfig;
	private String                                        windowTitle;
	private GameCycleIterationType                        gameCycleIterationType;
	private int                                           gameCycleUrMult;
	private long                                          gameCycleMaxUpdateTime;

	private boolean built;
	private GameCycleIterationFunc gameCycleIterationFunc;

	public GameConfig() {
		this.worldDefs              = new ArrayList<>();
		this.initWorlds             = new ArrayList<>();
		this.graphicsConfig         = new GraphicsConfig(DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT);
		this.gameCycleIterationType = DEFAULT_GAME_CYCLE_ITERATION_TYPE;
		this.gameCycleUrMult        = DEFAULT_UR_MULT;
		this.gameCycleMaxUpdateTime = DEFAULT_MAX_UPDATE_TIME;

		this.built                  = false;
		this.gameCycleIterationFunc = null;
	}

	private GameConfig(ArrayList<WorldDef> worldDefs, ArrayList<Pair<WorldDef, WorldInitWorldData>> initWorlds, GraphicsConfig graphicsConfig, String windowTitle, GameCycleIterationType gameCycleIterationType, int gameCycleUrMult, long gameCycleMaxUpdateTime, boolean built) {
		this.worldDefs              = worldDefs;
		this.initWorlds             = initWorlds;
		this.graphicsConfig         = graphicsConfig;
		this.windowTitle            = windowTitle;
		this.gameCycleIterationType = gameCycleIterationType;
		this.gameCycleUrMult        = gameCycleUrMult;
		this.gameCycleMaxUpdateTime = gameCycleMaxUpdateTime;

		this.built = built;
	}

	//**

	private void ensureNotBuilt() {
		if (this.built) {
			throw new RuntimeException("cannot modify already built game configuration");
		}
	}

	public GameConfig build() {
		this.built = true;
		this.gameCycleIterationFunc = GameManager.getGameCycleIterationFunc(this.gameCycleIterationType, this.gameCycleUrMult, this.gameCycleMaxUpdateTime);

		return this;
	}

	public boolean isBuilt() {
		return this.built;
	}

	//**

	ArrayList<WorldDef> getWorldDefs() {
		return this.worldDefs;
	}

	ArrayList<Pair<WorldDef, WorldInitWorldData>> getInitWorlds() {
		return this.initWorlds;
	}

	public GraphicsConfig getGraphicsConfig() {
		return this.graphicsConfig;
	}

	public String getWindowTitle() {
		return this.windowTitle;
	}

	public GameCycleIterationType getGameCycleIterationType() {
		return this.gameCycleIterationType;
	}

	public int getGameCycleUrMult() {
		return this.gameCycleUrMult;
	}

	public long getGameCycleMaxUpdateTime() {
		return this.gameCycleMaxUpdateTime;
	}

	GameCycleIterationFunc getGameCycleIterationFunc() {
		return this.gameCycleIterationFunc;
	}

	//**

	public GameConfig setWorldDefs(WorldDef... worldDefs) {
		this.ensureNotBuilt();
		this.worldDefs = new ArrayList<>();
		Collections.addAll(this.worldDefs, worldDefs);

		return this;
	}

	public GameConfig setWorlds(Pair<WorldDef, WorldInitWorldData>... initWorlds) {
		this.ensureNotBuilt();
		this.worldDefs = new ArrayList<>();
		Collections.addAll(this.initWorlds, initWorlds);

		return this;
	}

	public GameConfig setGraphicsConfig(GraphicsConfig graphicsConfig) {
		this.graphicsConfig = graphicsConfig;
		return this;
	}

	public GameConfig setWindowTitle(String windowTitle) {
		this.ensureNotBuilt();
		this.windowTitle = windowTitle;

		return this;
	}

	public GameConfig setGameCycleIterationType(GameCycleIterationType gameCycleIterationType) {
		this.ensureNotBuilt();
		this.gameCycleIterationType = gameCycleIterationType;

		return this;
	}

	public GameConfig setGameCycleUrMult(int gameCycleUrMult) {
		this.ensureNotBuilt();
		this.gameCycleUrMult = gameCycleUrMult;

		return this;
	}

	public GameConfig setGameCycleMaxUpdateTime(long gameCycleMaxUpdateTime) {
		this.ensureNotBuilt();
		this.gameCycleMaxUpdateTime = gameCycleMaxUpdateTime;

		return this;
	}

	//**

	@Override
	public GameConfig copy() {
		return new GameConfig(
			UtilFunctions.copyArrayList(this.worldDefs),
			UtilFunctions.copyPairArrayList(this.initWorlds),
			this.graphicsConfig.copy(),
			this.windowTitle,
			this.gameCycleIterationType,
			this.gameCycleUrMult,
			this.gameCycleMaxUpdateTime,
			this.built
		);
	}

	@Override
	public String toString() {
		return
			"{ "                             + UtilFunctions.stringifyArrayList("world-defs", this.worldDefs)   +
			", "                             + UtilFunctions.stringifyArrayList("init-worlds", this.initWorlds) +
			", graphics-config: "            + this.graphicsConfig                                              +
			", window-title: "               + this.windowTitle                                                 +
			", game-cycle-iter-type: "       + this.gameCycleIterationType                                      +
			", game-cycle-ur-mult: "         + this.gameCycleUrMult                                             +
			", game-cycle-max-update-time: " + this.gameCycleMaxUpdateTime                                      + " }";
	}
}