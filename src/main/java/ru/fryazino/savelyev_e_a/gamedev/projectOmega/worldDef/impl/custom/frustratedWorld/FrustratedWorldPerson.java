package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.impl.custom.frustratedWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Camera2D;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Drawable2D;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.GraphicsAdapter;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.elements.animatedModel.AnimatedModel;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.Person;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.outWorld.Human;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.IPoint;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.Point;

public final class FrustratedWorldPerson extends Person implements Drawable2D {
	public static final int PERSON_SURFACE_HEIGHT = 65;

	private static final float START_SPEED = 1;

	private final WorldGravity  gravityLink;
	private       Point         pos;
	private       Point         speed;
	private       Point         acceleration;
	private final AnimatedModel animatedModel;
	private       boolean       lookLeft;

	FrustratedWorldPerson(Human humanRef, Point pos, WorldGravity gravityLink, AnimatedModel animatedModel) {
		this(humanRef, pos, gravityLink, new Point(0, START_SPEED), new Point(0, -gravityLink.getFallAcceleration()), animatedModel);
	}

	FrustratedWorldPerson(Human humanRef, Point pos, WorldGravity gravityLink, Point speed, Point acceleration, AnimatedModel animatedModel) {
		super(humanRef);

		this.gravityLink   = gravityLink;
		this.pos           = pos;
		this.speed         = speed;
		this.acceleration  = acceleration;
		this.animatedModel = animatedModel;
		this.lookLeft      = false;
	}

	//**

	public WorldGravity getGravityLink() {
		return this.gravityLink;
	}

	public Point getPos() {
		return this.pos;
	}

	public Point getSpeed() {
		return this.speed;
	}

	public Point getAcceleration() {
		return this.acceleration;
	}

	public AnimatedModel getAnimatedModel() {
		return this.animatedModel;
	}

	public boolean isLookingLeft() {
		return this.lookLeft;
	}

	public void setPos(Point pos) {
		this.pos = pos;
	}

	public void setSpeed(Point speed) {
		this.speed = speed;
	}

	public void setAcceleration(Point acceleration) {
		this.acceleration = acceleration;
	}

	public void setLookingLeft(boolean value) {
		this.lookLeft = value;
	}

	//**

	private void processPhysics() {
		this.speed = this.speed.move(this.acceleration).mul(this.gravityLink.getSpeedCounteractionFactor().getX(), this.gravityLink.getSpeedCounteractionFactor().getY());

		if (Math.abs(this.speed.getX()) < this.gravityLink.getZeroSpeedThreshold()) {
			this.speed = this.speed.nulX();
		}

		if (Math.abs(this.speed.getY()) < this.gravityLink.getZeroSpeedThreshold()) {
			this.speed = this.speed.nulY();
		}

		if (this.pos.getY() <= PERSON_SURFACE_HEIGHT) {
			this.pos   = this.pos.changeY(PERSON_SURFACE_HEIGHT);
			this.speed = this.speed.changeY(Math.max(this.speed.getY(), 0));
		}

		this.pos = this.pos.move(this.speed);
	}

	void processTic() {
		this.processPhysics();
		this.animatedModel.tic();
	}

	//**

	@Override
	public void draw(GraphicsAdapter graphics, Camera2D camera, IPoint offset, Object... args) {
		this.animatedModel.draw(graphics, camera, offset, args);
	}

	@Override
	public String toString() {
		return "{ super: " + super.toString() + ", pos: " + this.pos.toString() + "gravity-link: " + this.gravityLink.toString() + ", speed: " + this.speed.toString() + ", accceleration: " + this.acceleration.toString() + ", animated-model: " + this.animatedModel.toString() + ", look-left: " + this.lookLeft + " }";
	}
}