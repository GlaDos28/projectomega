package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.KeyboardState;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.Person;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;

@FunctionalInterface
public interface KeyListener<WD extends WorldData> {
	void process(KeyboardState state, WD worldData, Person person);
}
