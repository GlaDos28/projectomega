package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldUpdatePerson;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;

@FunctionalInterface
public interface IWorldUpdatePerson<WD extends WorldData> {
	void updatePerson(WorldUpdatePersonData<WD> data);
}