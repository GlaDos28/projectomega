package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldInitPerson;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.Person;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;

@FunctionalInterface
public interface IWorldInitPerson<WD extends WorldData> {
	Person initPerson(WorldInitPersonData<WD> data);
}