package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces;

import java.io.Serializable;

public interface CSD<T> extends Copyable<T>, Serializable { /* Cloneable, Serializable, Displayable */
	@Override
	String toString();
}