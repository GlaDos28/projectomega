package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.outWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.Controller;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.game.GraphicsTransmitData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.World;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.UtilFunctions;

import java.util.ArrayList;
import java.util.Collections;

public final class Universe implements CSD<Universe> {
	static final int NO_WORLD = -1;

	private final ArrayList<World> worlds;
	private final Human            human;
	private       int              humanPrevWorldInd;

	public Universe() {
		this.worlds            = new ArrayList<>();
		this.human             = new Human();
		this.humanPrevWorldInd = NO_WORLD;
	}

	private Universe(ArrayList<World> worlds, Human human, int humanPrevWorldInd) {
		this.worlds            = worlds;
		this.human             = human;
		this.humanPrevWorldInd = humanPrevWorldInd;
	}

	//**

	public Human getHuman() {
		return this.human;
	}

	//**

	public void enterWorld(int worldIndex) {
		this.human.setWorldIndex(worldIndex);
	}

	public void addWorlds(World... worlds) {
		Collections.addAll(this.worlds, worlds);
	}

	public void execTic(Controller controller) {
		if (this.human.getWorldIndex() != this.humanPrevWorldInd) {
			/* closing the previous world */

			if (this.humanPrevWorldInd != NO_WORLD) {
				worlds.get(this.humanPrevWorldInd).closePerson();
				worlds.get(this.humanPrevWorldInd).closeWorld();
			}

			/* entering the new world */

			if (this.human.getWorldIndex() != NO_WORLD) {
				World world = worlds.get(this.human.getWorldIndex());

				world.updatePerson();
				world.updateWorld(System.currentTimeMillis() - world.getLastTimePoint());
				world.execTic(controller);
			}
		} else if (this.human.getWorldIndex() != NO_WORLD) {
			worlds.get(this.human.getWorldIndex()).execTic(controller);
		}

		this.humanPrevWorldInd = this.human.getWorldIndex();
	}

	public void render(GraphicsTransmitData graphicsData, Controller controller) {
		worlds.get(this.human.getWorldIndex()).render(graphicsData, controller);
	}

	//**

	@Override
	public Universe copy() {
		return new Universe(UtilFunctions.copyArrayList(this.worlds), this.human.copy(), this.humanPrevWorldInd);
	}

	@Override
	public String toString() {
		return "{ " + UtilFunctions.stringifyArrayList("worlds", this.worlds) + ", inWorld: " + this.human.toString() + ", inWorld-prev-outWorld-ind: " + this.humanPrevWorldInd + " }";
	}
}