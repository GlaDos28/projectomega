package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.Controller;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.game.GraphicsTransmitData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.WorldDef;

public final class World<WD extends WorldData> implements CSD<World<WD>> {
	private final WorldDef<WD>       worldDef;
	private final WD                 worldData;
	private final Person             person;
	private       long               lastTimePoint;
	private final WorldExtraData<WD> extraData;

	public World(WorldDef<WD> worldDef, WD worldData, Person person, long initialTimePoint, WorldExtraData<WD> extraData) {
		this.worldDef      = worldDef;
		this.worldData     = worldData;
		this.person        = person;
		this.lastTimePoint = initialTimePoint;
		this.extraData     = extraData;
	}

	//**

	public WD getWorldData() {
		return this.worldData;
	}

	public Person getPerson() {
		return this.person;
	}

	public long getLastTimePoint() {
		return this.lastTimePoint;
	}

	public WorldExtraData<WD> getExtraData() {
		return this.extraData;
	}

	//**

	public void execTic(Controller controller) {
		this.worldDef.execTic(this, controller);
	}

	public void render(GraphicsTransmitData graphicsData, Controller controller) {
		this.worldDef.render(this.worldData, this.person, graphicsData, controller);
	}

	public void updatePerson() {
		this.worldDef.updatePerson(this.person);
	}

	public void updateWorld(long timePassed) {
		this.worldDef.updateWorld(this.worldData, timePassed);
		this.lastTimePoint += timePassed;
	}

	public void closePerson() {
		this.worldDef.closePerson(this.person);
	}

	public void closeWorld() {
		this.worldDef.closeWorld(this.worldData);
	}

	public WorldDef getWorldDef() {
		return this.worldDef;
	}

	//**

	@Override
	public World<WD> copy() {
		return new World<>(this.worldDef.copy(), (WD) this.worldData.copy(), this.person.copy(), this.lastTimePoint, this.extraData.copy());
	}

	@Override
	public String toString() {
		return "{ world-def: " + this.worldDef.toString() + ", world-data: " + this.worldData.toString() + ", world-extra-data: " + this.extraData.toString() + " }";
	}
}