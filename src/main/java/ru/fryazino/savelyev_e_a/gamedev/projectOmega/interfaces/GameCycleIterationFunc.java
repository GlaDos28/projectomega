package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.Container;

@FunctionalInterface
public interface GameCycleIterationFunc {
	void processCycleIteration(Container<Boolean> isRunning, Container<GameUpdateFunc> updateFunc, Container<GameRenderFunc> renderFunc);
}
