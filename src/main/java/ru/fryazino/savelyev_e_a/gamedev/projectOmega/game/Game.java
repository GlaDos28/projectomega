package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.game;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.Controller;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.GraphicsAdapter;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.outWorld.Universe;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.World;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.GameRenderFunc;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.GameUpdateFunc;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldInitPerson.WorldInitPersonData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.Container;

import java.awt.*;
import java.awt.image.BufferStrategy;

public final class Game extends Canvas implements Runnable {
	private static final long serialVersionUID = 1L;

	private final Container<Boolean>        running;
	private final Container<GameUpdateFunc> updateFunc;
	private final Container<GameRenderFunc> renderFunc;

	private final Container<Frame> frame;
	private final GameConfig       config;
	private final Universe         universe;
	private final Controller       controller;

	public Game(Container<Frame> frame, GameConfig config) {
		if (config.getWorldDefs() == null)
			throw new RuntimeException("configuration property \"world-defs\" must not be null");

		this.running    = new Container<>(false);
		this.updateFunc = new Container<>();
		this.renderFunc = new Container<>();

		this.frame      = frame;
		this.config     = config;
		this.universe   = new Universe();
		this.controller = new Controller();
	}

	//**

	public Controller getController() {
		return this.controller;
	}

	//**

	public void start() {
		this.running.set(true);
		new Thread(this).start();
	}

	public void stop() {
		this.running.set(false);
	}

	public void enterWorld(int worldIndex) { /* TODO remove this temporary method */
		this.universe.enterWorld(worldIndex);
	}

	@Override
	public void run() {
		this.init();
		this.config.getGameCycleIterationFunc().processCycleIteration(this.running, this.updateFunc, this.renderFunc);

		this.frame.get().dispose();
		System.out.println("The program has been exited successfully.");
	}

	private void init() {
		this.updateFunc.set(() -> {
			this.universe.execTic(this.controller);
			this.controller.getKeyboardController().getState().update();
		});

		this.renderFunc.set(() -> {
			BufferStrategy bs = getBufferStrategy();

			if (bs == null) {
				createBufferStrategy(2);
				requestFocus();
			} else {
				Graphics graphics = bs.getDrawGraphics();

				this.universe.render(new GraphicsTransmitData(new GraphicsAdapter(graphics, this.config.getGraphicsConfig()), super.getWidth(), super.getHeight()), this.controller);

				graphics.dispose();
				bs.show();
			}
		});

		this.universe.addWorlds((World[]) this.config.getInitWorlds().stream().map((worldData) -> worldData.getKey().generateWorld(
			this,
			worldData.getValue(),
			new WorldInitPersonData(this.universe.getHuman()),
			System.currentTimeMillis())
		).toArray(World[]::new));

		super.addKeyListener(this.controller.getKeyboardController());
	}

	@Override
	public String toString() {
		return "{ config: " + this.config.toString() + ", universe: " + this.universe.toString() + ", is-running: " + this.running.toString() + ", controller: " + this.controller.toString() + " }";
	}
}
