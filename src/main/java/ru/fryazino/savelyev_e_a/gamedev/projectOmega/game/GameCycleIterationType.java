package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.game;

public enum GameCycleIterationType {
	FAST,
	UNIFORM,
	COMBINED
}