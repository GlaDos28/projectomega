package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.Controller;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra.KeyProcessType;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra.KeyProcessor;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra.KeyboardKeyStandardProcessing;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra.KeyboardStandardProcessor;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.game.Game;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.game.GraphicsTransmitData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.Person;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.World;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldExtraData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldClosePerson.IWorldClosePerson;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldClosePerson.WorldClosePersonData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldCloseWorld.IWorldCloseWorld;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldCloseWorld.WorldCloseWorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldExecTic.IWorldExecTic;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldExecTic.WorldExecTicData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldInitPerson.IWorldInitPerson;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldInitPerson.WorldInitPersonData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldInitWorld.IWorldInitWorld;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldInitWorld.WorldInitWorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldRender.IWorldRender;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldRender.WorldRenderData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldUpdatePerson.IWorldUpdatePerson;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldUpdatePerson.WorldUpdatePersonData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldUpdateWorld.IWorldUpdateWorld;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldUpdateWorld.WorldUpdateWorldData;

import java.awt.event.KeyEvent;

public final class WorldDef<WD extends WorldData> implements CSD<WorldDef<WD>> {
	private final IWorldInitWorld<WD>    initWorld;
	private final IWorldInitPerson<WD>   initPerson;
	private final IWorldExecTic<WD>      execTic;
	private final IWorldRender<WD>       render;
	private final IWorldUpdatePerson<WD> updatePerson;
	private final IWorldUpdateWorld<WD>  updateWorld;
	private final IWorldClosePerson<WD>  closePerson;
	private final IWorldCloseWorld<WD>   closeWorld;


	public WorldDef(IWorldInitWorld<WD> initWorld, IWorldInitPerson<WD> initPerson , IWorldExecTic<WD> execTic, IWorldRender<WD> render, IWorldUpdatePerson<WD> updatePerson, IWorldUpdateWorld<WD> updateWorld, IWorldClosePerson<WD> closePerson, IWorldCloseWorld<WD> closeWorld) {
		this.initWorld    = initWorld;
		this.initPerson   = initPerson;
		this.execTic      = execTic;
		this.render       = render;
		this.updatePerson = updatePerson;
		this.updateWorld  = updateWorld;
		this.closePerson  = closePerson;
		this.closeWorld   = closeWorld;
	}

	public World<WD> generateWorld(final Game gameLink, WorldInitWorldData<WD> initWorldData, WorldInitPersonData<WD> initPersonData, long initialTimePoint) {
		KeyboardStandardProcessor<WD> keyboardStandardProcessor = new KeyboardStandardProcessor<>();

		if (initWorldData instanceof KeyboardKeyStandardProcessing) {
			KeyboardKeyStandardProcessing<WD> keyboardKeyStandardProcessing = (KeyboardKeyStandardProcessing<WD>) initWorldData;

			for (KeyProcessor<WD> keyProcessor : keyboardKeyStandardProcessing.getKeyProcessors()) {
				if (keyProcessor.getKeyCode() != KeyEvent.VK_ESCAPE) {
					keyboardStandardProcessor.addKeyProcessor(keyProcessor);
				}
			}

			keyboardStandardProcessor.addKeyProcessor(new KeyProcessor<>(KeyProcessType.ONE_TIME, KeyEvent.VK_ESCAPE, (key, data) -> gameLink.stop(), -1)); /* TODO put all instead of -1 */
		}

		WD worldData = this.initWorld.initWorld(initWorldData);
		worldData.setPerson(this.initPerson.initPerson(initPersonData));

		return new World<>(this, worldData, worldData.getPerson(), initialTimePoint, new WorldExtraData<>(keyboardStandardProcessor));
	}

	public void execTic(World<WD> world, Controller controller) {
		this.execTic.exec(new WorldExecTicData<>(world, controller));
	}

	public void render(WD worldData, Person person, GraphicsTransmitData graphicsData, Controller controller) {
		this.render.render(new WorldRenderData<>(worldData, person, graphicsData, controller));
	}

	public void updatePerson(Person person) {
		this.updatePerson.updatePerson(new WorldUpdatePersonData<>(person));
	}

	public void updateWorld(WD worldData, long timePassed) {
		this.updateWorld.updateWorld(new WorldUpdateWorldData<>(worldData, timePassed));
	}

	public void closePerson(Person person) {
		this.closePerson.closePerson(new WorldClosePersonData<>(person));
	}

	public void closeWorld(WD worldData) {
		this.closeWorld.closeWorld(new WorldCloseWorldData<>(worldData));
	}

	@Override
	public WorldDef<WD> copy() {
		return this; /* immutable object */
	}

	@Override
	public String toString() {
		return
			"{ init-world: "    + this.initWorld.toString()    +
			"{ init-person: "   + this.initWorld.toString()    +
			", execTic: "       + this.execTic.toString()      +
			", render: "        + this.render.toString()       +
			", update-person: " + this.updatePerson.toString() +
			", update-world: "  + this.updateWorld.toString()  +
			", close-person: "  + this.closePerson.toString()  +
			", close-world: "   + this.closeWorld.toString()   +
			" }";
	}
}