package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.game;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.GameCycleIterationFunc;

import javax.swing.*;
import java.awt.*;

public final class GameManager {
	private GameManager() {}

	static GameCycleIterationFunc getGameCycleIterationFunc(GameCycleIterationType type, int urMult, long maxUpdateTime) {
		switch (type) {
			case FAST:
				return (isRunning, updateFunc, renderFunc) -> {
					renderFunc.get().render();

					while (isRunning.get()) {
						long timeLimit = System.currentTimeMillis() + maxUpdateTime * urMult;

						for (int i = 0; i < urMult && System.currentTimeMillis() < timeLimit; i++) {
							updateFunc.get().update();
						}

						long timeLeft = timeLimit - System.currentTimeMillis();

						if (timeLeft > 0) {
							try {
								Thread.sleep(timeLeft);
							} catch (InterruptedException ex) {
								return;
							}
						}

						renderFunc.get().render();
					}
				};
			case UNIFORM:
				return (isRunning, updateFunc, renderFunc) -> {
					renderFunc.get().render();

					while (isRunning.get()) {
						long totalTimeLimit = System.currentTimeMillis() + maxUpdateTime * urMult;
						long timeLeft;

						for (int i = 0; i < urMult && System.currentTimeMillis() < totalTimeLimit; i++) {
							long timeLimit = System.currentTimeMillis() + maxUpdateTime;

							updateFunc.get().update();

							timeLeft = timeLimit - System.currentTimeMillis();

							if (timeLeft > 0) {
								try {
									Thread.sleep(timeLeft);
								} catch (InterruptedException ex) {
									return;
								}
							}
						}

						timeLeft = totalTimeLimit - System.currentTimeMillis();

						if (timeLeft > 0) {
							try {
								Thread.sleep(timeLeft);
							} catch (InterruptedException ex) {
								return;
							}
						}

						renderFunc.get().render();
					}
				};
			case COMBINED:
				return (isRunning, updateFunc, renderFunc) -> {
					renderFunc.get().render();

					while (isRunning.get()) {
						long timeLimit = System.currentTimeMillis();
						long totalTimeLimit = timeLimit + maxUpdateTime * urMult;
						long timeLeft;

						for (int i = 0; i < urMult && System.currentTimeMillis() < totalTimeLimit; i++) {
							timeLimit += maxUpdateTime;

							updateFunc.get().update();

							timeLeft = timeLimit - System.currentTimeMillis();

							if (timeLeft > 0) {
								try {
									Thread.sleep(timeLeft);
								} catch (InterruptedException ex) {
									return;
								}
							}
						}

						timeLeft = totalTimeLimit - System.currentTimeMillis();

						if (timeLeft > 0) {
							try {
								Thread.sleep(timeLeft);
							} catch (InterruptedException ex) {
								return;
							}
						}

						renderFunc.get().render();
					}
				};
			default:
				throw new RuntimeException("unknown game cycle iteration type " + type);
		}
	}

	public static GameFacade createGameInstance(GameConfig gameConfig) {
		if (gameConfig == null || !gameConfig.isBuilt()) {
			throw new RuntimeException("nullable or not built game configuration");
		}

		main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.Container<Frame> frameContainer = new main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.Container<>();
		Game game = new Game(frameContainer, gameConfig);

		game.setPreferredSize(new Dimension(gameConfig.getGraphicsConfig().getScrWidth(), gameConfig.getGraphicsConfig().getScrHeight()));

		JFrame frame = new JFrame(gameConfig.getWindowTitle());
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(game, BorderLayout.CENTER);
		frame.pack();
		frame.setResizable(false);
		frame.setVisible(true);

		frameContainer.set(frame);

		return new GameFacade(game);
	}

	@Override
	public String toString() {
		return "{ game-manager }";
	}
}
