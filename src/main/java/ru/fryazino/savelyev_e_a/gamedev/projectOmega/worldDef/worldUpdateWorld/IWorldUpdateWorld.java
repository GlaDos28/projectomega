package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldUpdateWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;

@FunctionalInterface
public interface IWorldUpdateWorld<WD extends WorldData> {
	void updateWorld(WorldUpdateWorldData<WD> data);
}