package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.worldElements.multiLevel2DBackground;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Drawable2D;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.UtilFunctions;

import java.util.ArrayList;
import java.util.function.Function;

public final class MultiLevel2DBackgroundDef {
	private final Drawable2D                    backDrawable;
	private final int                           memorizeMaxDist;
	private final int                           updateMinDist;
	private final int                           maxHeight;
	private final MultiLevel2DBackgroundGenFunc genFunc;
	private final Function<Integer, Float>      heightToScale;
	private final Function<Integer, Float>      heightToMovScale;

	public MultiLevel2DBackgroundDef(Drawable2D backDrawable, int memorizeMaxDist, int updateMinDist, int maxHeight, MultiLevel2DBackgroundGenFunc genFunc, Function<Integer, Float> heightToScale, Function<Integer, Float> heightToMovScale) {
		this.backDrawable     = backDrawable;
		this.memorizeMaxDist  = memorizeMaxDist;
		this.updateMinDist    = updateMinDist;
		this.maxHeight        = maxHeight;
		this.genFunc          = genFunc;
		this.heightToScale    = heightToScale;
		this.heightToMovScale = heightToMovScale;
	}

	public MultiLevel2DBackgroundDef(Drawable2D backDrawable, int memorizeMaxDist, int updateMinDist, int maxHeight, float minScale /* [0, 1] */, double density, MultiLevel2DBackgroundObjectGen... objectGenerators) {
		this.backDrawable     = backDrawable;
		this.memorizeMaxDist  = memorizeMaxDist;
		this.updateMinDist    = updateMinDist;
		this.maxHeight        = maxHeight;

		this.heightToScale = (height) -> (1 - minScale) * (1 - (float) height / maxHeight) + minScale;

		this.heightToMovScale = (height) -> 1 - (float) height / maxHeight;

		this.genFunc = (l, r, maxH) -> {
			ArrayList<MultiLevel2DBackgroundObject> objects = new ArrayList<>();

			int genObjNum = (int) (density * (r - l + 1));

			for (int i = 0; i < genObjNum; i++) {
				objects.add(new MultiLevel2DBackgroundObject(objectGenerators[(int) (Math.random() * objectGenerators.length)], UtilFunctions.random(l, r), (int) (Math.random() * maxH)));
			}

			/*for (int i = l; i <= r; i++) {
				if (Math.random() <= density) {
					objects.add(new MultiLevel2DBackgroundObject(objectGenerators[(int) (Math.random() * objectGenerators.length)], i, (int) (Math.random() * maxH)));
				}
			}*/

			return objects;
		};
	}

	Drawable2D getBackgroundDrawable() {
		return this.backDrawable;
	}

	int getMemorizeMaxDist() {
		return this.memorizeMaxDist;
	}

	int getUpdateMinDist() {
		return this.updateMinDist;
	}

	public int getMaxHeight() {
		return this.maxHeight;
	}

	MultiLevel2DBackgroundGenFunc getGenFunc() {
		return this.genFunc;
	}

	Function<Integer, Float> getHeightToScaleFunc() {
		return this.heightToScale;
	}

	Function<Integer, Float> getHeightToMovScaleFunc() {
		return this.heightToMovScale;
	}

	@Override
	public String toString() {
		return "{ back-img: " + this.backDrawable.toString() + ", memorize-max-dist: " + this.memorizeMaxDist + ", update-min-dist: " + this.updateMinDist + ", max-height: " + this.maxHeight + ", gen-func: " + this.genFunc.toString() + ", height-to-scale-func: " + this.heightToScale.toString() + ", height-to-mov-scale-func: " + this.heightToMovScale.toString() + " }";
	}
}
