package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.impl.custom.testWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.impl.standard.CellField;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.UtilFunctions;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.IPoint;

public final class TestWorldData extends CellField {
	private IPoint personPos;

	public TestWorldData(int n, int m, IPoint personPos) {
		super(n, m);
		this.personPos = personPos;
	}

	private TestWorldData(int[][] cells, IPoint personPos) {
		super(cells);
		this.personPos = personPos;
	}

	//**

	public IPoint getPersonPos() {
		return this.personPos;
	}

	public void setPersonPos(IPoint personPos) {
		this.personPos = personPos;
	}

	//**

	@Override
	public TestWorldData copy() {
		return new TestWorldData(UtilFunctions.copyIntTable(super.cells), this.personPos.copy());
	}

	@Override
	public String toString() {
		return "{ java-world-data " + super.getN() + "x" + super.getM() + "\n" + UtilFunctions.stringifyIntTable(super.cells) + "\n}";
	}
}