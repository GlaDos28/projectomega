package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces;

@FunctionalInterface
public interface GameUpdateFunc {
	void update();
}
