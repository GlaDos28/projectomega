package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.game;

public final class GameFacade {
	private final Game gameInstance;

	GameFacade(Game gameInstance) {
		this.gameInstance = gameInstance;
	}

	public void start() {
		this.gameInstance.start();
	}

	public void stop() {
		this.gameInstance.stop();
	}

	public void enterWorld(int worldIndex) {
		this.gameInstance.enterWorld(worldIndex);
	}

	@Override
	public String toString() {
		return "{ game-instance: " + this.gameInstance.toString() + " }";
	}
}