package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldInitPerson;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.outWorld.Human;

public final class WorldInitPersonData<WD extends WorldData> implements CSD<WorldInitPersonData<WD>> {
	private final Human human;

	public WorldInitPersonData(Human human) {
		this.human = human;
	}

	public Human getHuman() {
		return this.human;
	}

	@Override
	public WorldInitPersonData<WD> copy() {
		return new WorldInitPersonData<>(this.human.copy());
	}

	@Override
	public String toString() {
		return "{ human: " + this.human.toString() + " }";
	}
}