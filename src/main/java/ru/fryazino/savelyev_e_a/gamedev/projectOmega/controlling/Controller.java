package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.KeyboardController;

public final class Controller { /* TODO mouse controller */
	private final KeyboardController keyboardController;

	public Controller() {
		this.keyboardController = new KeyboardController();
	}

	public KeyboardController getKeyboardController() {
		return this.keyboardController;
	}

	@Override
	public String toString() {
		return "{ keyboard-controller: " + this.keyboardController.toString() + " }";
	}
}