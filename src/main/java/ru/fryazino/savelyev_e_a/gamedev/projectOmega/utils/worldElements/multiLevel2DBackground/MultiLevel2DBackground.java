package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.worldElements.multiLevel2DBackground;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Camera2D;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Drawable2D;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.GraphicsAdapter;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.IPoint;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

public final class MultiLevel2DBackground implements Drawable2D {
	private final MultiLevel2DBackgroundDef definition;
	private final IPoint pos;
	private final TreeSet<MultiLevel2DBackgroundObject> objects;
	private float xOffset;
	private int   lastUpdateL;
	private int   lastUpdateR;

	public MultiLevel2DBackground(MultiLevel2DBackgroundDef definition, IPoint pos) {
		this.definition  = definition;
		this.pos         = pos;
		this.objects     = new TreeSet<>();
		this.xOffset     = 0;
		this.lastUpdateL = this.pos.getX();
		this.lastUpdateR = this.pos.getX();

		this.generateObjects(-this.definition.getMemorizeMaxDist(), this.definition.getMemorizeMaxDist());
	}

	private void generateObjects(int l, int r) {
		ArrayList<MultiLevel2DBackgroundObject> newObjects = this.definition.getGenFunc().generateBackground(l, r, this.definition.getMaxHeight());

		newObjects.forEach((object) -> object.setDrawable(this.definition.getHeightToScaleFunc().apply(object.getHeight())));

		this.objects.addAll(newObjects);
	}

	public void move(float amount) {
		this.xOffset -= amount;

		Iterator<MultiLevel2DBackgroundObject> iterator = this.objects.iterator();

		while (iterator.hasNext()) {
			MultiLevel2DBackgroundObject object = iterator.next();
			object.moveXPos((float) amount * this.definition.getHeightToMovScaleFunc().apply(object.getHeight()));

			if (object.getXPos() < -this.definition.getMemorizeMaxDist() || object.getXPos() > this.definition.getMemorizeMaxDist()) {
				iterator.remove();
			}
		}

		int xOffsetRounded = (int) xOffset;

		this.lastUpdateL = Math.max(this.lastUpdateL, xOffsetRounded);
		this.lastUpdateR = Math.min(this.lastUpdateR, xOffsetRounded);

		if (this.lastUpdateL - this.xOffset >= this.definition.getUpdateMinDist()) {
			this.generateObjects(-this.definition.getMemorizeMaxDist(), -this.definition.getMemorizeMaxDist() + this.definition.getUpdateMinDist());
			this.lastUpdateL = xOffsetRounded;
		}

		if (this.xOffset - this.lastUpdateR >= this.definition.getUpdateMinDist()) {
			this.generateObjects(this.definition.getMemorizeMaxDist() - this.definition.getUpdateMinDist(), this.definition.getMemorizeMaxDist());
			this.lastUpdateR = xOffsetRounded;
		}
	}

	@Override
	public void draw(GraphicsAdapter graphics, Camera2D camera, IPoint offset, Object... args) {
		this.definition.getBackgroundDrawable().draw(graphics, camera, this.pos.sum(offset));

		for (MultiLevel2DBackgroundObject backgroundObject : this.objects) {
			backgroundObject.getDrawable().draw(graphics, camera, new IPoint((int) backgroundObject.getXPos(), backgroundObject.getHeight()).sum(this.pos).sum(offset));
		}
	}
}
