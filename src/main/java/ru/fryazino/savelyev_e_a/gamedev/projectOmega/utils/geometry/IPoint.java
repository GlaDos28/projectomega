package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;

/**
 * Point with integer coordinates
 * 
 * @author GlaDos
 * @since 01.03.16
 */
public final class IPoint implements CSD<IPoint> {
    /**
     * X point coordinate
     * 
     * @since 01.03.16
     */
    private final int x;
    
    /**
     * Y point coordinate
     * 
     * @since 01.03.16
     */
    private final int y;
    
    /**
     * Full constructor with X and Y coordinates
     * 
     * @param x X point integer coordinate
     * @param y Y point integer coordinate
     * @since 01.03.16
     */
    public IPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point getPoint() {
        return new Point(this.x, this.y);
    }
    
    /**
     * Returns X coordinate
     * 
     * @return X point coordinate
     * @since 01.03.16
     */
    public int getX() {
        return this.x;
    }
    
    /**
     * Returns Y coordinate
     * 
     * @return Y point coordinate
     * @since 01.03.16
     */
    public int getY() {
        return this.y;
    }

    public IPoint changeX(int x) {
        return new IPoint(x, this.y);
    }

    public IPoint changeY(int y) {
        return new IPoint(this.x, y);
    }

    public IPoint sum(IPoint p) {
        return new IPoint(this.x + p.x, this.y + p.y);
    }

    public IPoint sum(int x, int y) {
        return new IPoint(this.x + x, this.y + y);
    }

    public IPoint sumX(int x) {
        return new IPoint(this.x + x, this.y);
    }

    public IPoint sumY(int y) {
        return new IPoint(this.x, this.y + y);
    }

    public IPoint dif(IPoint p) {
        return new IPoint(this.x - p.x, this.y - p.y);
    }

    public IPoint difX(int x) {
        return new IPoint(this.x - x, this.y);
    }

    public IPoint difY(int y) {
        return new IPoint(this.x, this.y - y);
    }

    public IPoint neg() {
        return new IPoint(-this.x, -this.y);
    }

    public IPoint negX() {
        return new IPoint(-this.x, this.y);
    }

    public IPoint negY() {
        return new IPoint(this.x, -this.y);
    }

    public IPoint nul() {
        return new IPoint(0, 0);
    }

    public IPoint nulX() {
        return new IPoint(0, this.y);
    }

    public IPoint nulY() {
        return new IPoint(this.x, 0);
    }

    @Override
    public IPoint copy() {
        return this; /* immutable object */
    }
    
    /**
     * Returns string representation of this integer point
     * 
     * @return integer point represented by a string
     * @since 01.03.16
     */
    @Override
    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }
    
    /**
     * Whether the integer point equals to the given
     * 
     * @param p integer point to check with
     * @return boolean decision
     * @since 01.03.16
     */
    @Override
    public boolean equals(Object p) {
        return (p instanceof IPoint) && (this.x == ((IPoint)p).x) && (this.y == ((IPoint)p).y);
    }
    
    /**
     * Returns integer point's hash code
     * 
     * @return hash code of this integer point
     * @since 01.03.16
     */
    @Override
    public int hashCode() {
        int hash = 7;
        
        hash = 43 * hash + this.x;
        hash = 43 * hash + this.y;
        
        return hash;
    }
}
