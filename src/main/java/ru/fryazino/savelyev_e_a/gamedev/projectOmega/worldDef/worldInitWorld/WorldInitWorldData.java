package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldInitWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;

public class WorldInitWorldData<WD extends WorldData> implements CSD<WorldInitWorldData<WD>> {
	public WorldInitWorldData() {}

	@Override
	public WorldInitWorldData<WD> copy() {
		return this;
	}

	@Override
	public String toString() {
		return "{ world-init-data }";
	}
}