package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra;

public enum KeyProcessType {
	CONTINUOUS,
	ONE_TIME,
	TYPING
}