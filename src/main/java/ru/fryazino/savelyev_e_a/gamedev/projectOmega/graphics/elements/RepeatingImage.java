package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.elements;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Camera2D;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Drawable2D;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.GraphicsAdapter;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.IPoint;


public final class RepeatingImage implements Drawable2D {
	private final Image   image;
	private final boolean repeatX, repeatY;

	public RepeatingImage(Image image, boolean repeatX, boolean repeatY) {
		this.image   = image;
		this.repeatX = repeatX;
		this.repeatY = repeatY;
	}

	//**

	public Image getImage() {
		return this.image;
	}

	public boolean isRepeatingX() {
		return this.repeatX;
	}

	public boolean isRepeatingY() {
		return this.repeatY;
	}

	//**

	@Override
	public void draw(GraphicsAdapter graphics, Camera2D camera, IPoint pos, Object... args) { /* args[0-1] - image args; args[2] - IPoint of image location, args[3] - IPoint of repeat bound size */
		IPoint imgLoc = (IPoint) args[2];
		IPoint bound  = (IPoint) args[3];

		if (this.repeatX) {
			int x = imgLoc.getX() % this.image.getWidth() - this.image.getWidth() - bound.getX() / 2;

			while (x < bound.getX()) {
				this.image.draw(graphics, new Camera2D(camera.getPos().nulX()), pos.changeX(x), args);
				x += this.image.getWidth();
			}
		}

		if (this.repeatY) {
			int y = imgLoc.getY() % this.image.getHeight() - this.image.getHeight() - bound.getY() / 2;

			while (y < bound.getY()) {
				this.image.draw(graphics, new Camera2D(camera.getPos().nulY()), pos.changeY(y), args);
				y += this.image.getHeight();
			}
		}
	}

	@Override
	public String toString() {
		return "{ image: " + this.image.toString() + " }";
	}
}