package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils;

import javafx.util.Pair;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.Copyable;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

public final class UtilFunctions {
	private UtilFunctions() {}

	//**

	public static int posMod(int x, int mod) {
		int res = x % mod;
		return (res < 0) ? res + x : res;
	}

	//**

	public static int random(int n) { /* [0, n - 1] */
		return (int) (Math.random() * n);
	}

	public static int random(int l, int r) { /* [l, r] */
		return UtilFunctions.random(r - l + 1) + l;
	}

	//**

	public static BufferedImage loadImage(String imgPath) {
		try {
			return ImageIO.read(new File(imgPath));
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	//**

	public static <T extends Copyable<T>> ArrayList<T> copyArrayList(ArrayList<T> arr) {
		ArrayList<T> copy = new ArrayList<>();

		for (T elem : arr)
			copy.add(elem.copy());

		return copy;
	}

	public static <T1 extends Copyable<T1>, T2 extends Copyable<T2>> ArrayList<Pair<T1, T2>> copyPairArrayList(ArrayList<Pair<T1, T2>> arr) {
		ArrayList<Pair<T1, T2>> copy = new ArrayList<>();

		for (Pair<T1, T2> elem : arr)
			copy.add(new Pair<>(elem.getKey().copy(), elem.getValue().copy()));

		return copy;
	}

	public static ArrayList<Integer> copyIntArrayList(ArrayList<Integer> arr) {
		ArrayList<Integer> copy = new ArrayList<>();
		copy.addAll(arr);

		return copy;
	}

	public static ArrayList<Boolean> copyBoolArrayList(ArrayList<Boolean> arr) {
		ArrayList<Boolean> copy = new ArrayList<>();
		copy.addAll(arr);

		return copy;
	}

	//**

	public static int[][] copyIntTable(int[][] table) {
		int[][] res = new int[table.length][];

		for (int i = 0; i < table.length; i++) {
			res[i] = new int[table[i].length];
			System.arraycopy(table[i], 0, res[i], 0, table[i].length);
		}

		return res;
	}

	//**

	public static <T extends Copyable<T>> Set<T> copySet(Set<T> set) {
		Set<T> copy = new HashSet<>();

		for (T elem : set) {
			copy.add(elem.copy());
		}

		return copy;
	}

	public static Set<Integer> copyIntSet(Set<Integer> set) {
		Set<Integer> copy = new HashSet<>();
		copy.addAll(set);

		return copy;
	}

	public static Set<Boolean> copyBoolSet(Set<Boolean> set) {
		Set<Boolean> copy = new HashSet<>();
		copy.addAll(set);

		return copy;
	}

	public static Set<Character> copyCharSet(Set<Character> set) {
		Set<Character> copy = new HashSet<>();
		copy.addAll(set);

		return copy;
	}

	//**

	public static Queue<Integer> copyIntQueue(Queue<Integer> queue) {
		Queue<Integer> copy = new LinkedList<>();
		copy.addAll(queue);

		return copy;
	}

	public static Queue<Character> copyCharQueue(Queue<Character> queue) {
		Queue<Character> copy = new LinkedList<>();
		copy.addAll(queue);

		return copy;
	}

	//**

	public static <T> String stringifyArray(String name, T[] arr) {
		if (arr.length == 0) {
			return name + ": []";
		}

		StringBuilder builder = new StringBuilder()
			.append(name)
			.append(": [ ")
			.append(arr[0].toString());

		for (int i = 1; i < arr.length; i++) {
			builder.append(", ").append(arr[i].toString());
		}

		return builder.append(" ]").toString();
	}

	//**

	public static <T> String stringifyArrayList(String name, ArrayList<T> arr) {
		if (arr.isEmpty()) {
			return name + ": []";
		}

		StringBuilder builder = new StringBuilder()
			.append(name)
			.append(": [ ")
			.append(arr.get(0).toString());

		for (int i = 1; i < arr.size(); i++) {
			builder.append(", ").append(arr.get(i).toString());
		}

		return builder.append(" ]").toString();
	}

	public static String stringifyIntTable(int[][] table) {
		int max = Integer.MIN_VALUE;

		for (int[] row : table) {
			for (int elem : row) {
				max = Math.max(max, elem);
			}
		}

		int maxSymbolNum = String.valueOf(max).length();

		StringBuilder builder = new StringBuilder();

		for (int[] row : table) {
			for (int elem : row) {
				String elemStr = String.valueOf(elem);
				builder.append(elemStr);

				for (int i = 0, n = maxSymbolNum - elemStr.length(); i <= n; i++) {
					builder.append(' ');
				}
			}

			builder.deleteCharAt(builder.length() - 1).append('\n'); /* removing redundant last white space */
		}

		return builder.deleteCharAt(builder.length() - 1).toString(); /* removing redundant last newline */
	}

	public static <K, V> String stringifyMap(String name, Map<K, V> map) {
		if (map.isEmpty()) {
			return name + ": {}";
		}

		Iterator<Map.Entry<K, V>> iterator = map.entrySet().iterator();
		Map.Entry<K, V> first = iterator.next();

		StringBuilder builder = new StringBuilder()
			.append(name)
			.append(": { (")
			.append(first.getKey().toString())
			.append(", ")
			.append(first.getValue().toString())
			.append(')');

		while (iterator.hasNext()) {
			Map.Entry<K, V> entry = iterator.next();;
			builder
				.append(", (")
				.append(entry.getKey().toString())
				.append(", ")
				.append(entry.getValue().toString())
				.append(')');
		}

		return builder.append(" }").toString();
	}

	public static <T> String stringifyCollection(String name, Collection<T> collection) {
		if (collection.isEmpty()) {
			return name + ": {}";
		}

		Iterator<T> iterator = collection.iterator();

		StringBuilder builder = new StringBuilder()
			.append(name)
			.append(": { ")
			.append(iterator.next().toString());

		while (iterator.hasNext()) {
			builder.append(", ").append(iterator.next().toString());
		}

		return builder.append(" }").toString();
	}
}