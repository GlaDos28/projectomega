package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldExecTic;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;

@FunctionalInterface
public interface IWorldExecTic<WD extends WorldData> {
	void exec(WorldExecTicData<WD> data);
}