package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.impl.standard;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.UtilFunctions;

public class CellField extends WorldData {
	protected final int[][] cells;

	public CellField(int n, int m) {
		this.cells = new int[n][m];
	}

	protected CellField(int[][] cells) {
		this.cells = cells;
	}

	//**

	public int getN() {
		return this.cells.length;
	}

	public int getM() {
		return this.cells.length == 0 ? 0 : this.cells[0].length;
	}

	public int get(int line, int column) {
		return this.cells[line][column];
	}

	public CellField set(int line, int column, int value) {
		this.cells[line][column] = value;
		return this;
	}

	//**

	@Override
	public String toString() {
		return "{ cell-field " + this.getN() + "x" + this.getM() + "\n" + UtilFunctions.stringifyIntTable(this.cells) + "\n}";
	}

	@Override
	public CellField copy() {
		return new CellField(UtilFunctions.copyIntTable(this.cells));
	}
}