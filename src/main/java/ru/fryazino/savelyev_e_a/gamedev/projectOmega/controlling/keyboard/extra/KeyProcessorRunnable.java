package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldExecTic.WorldExecTicData;

@FunctionalInterface
public interface KeyProcessorRunnable<WD extends WorldData> {
	void processKey(int keyCodeOrChar, WorldExecTicData<WD> data);
}