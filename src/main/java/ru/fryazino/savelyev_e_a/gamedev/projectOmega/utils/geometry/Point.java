package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;

import static java.lang.Double.max;
import static java.lang.Math.acos;
import static java.lang.Math.cos;
import static java.lang.Math.signum;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

/* Wrong-made class: rewrite comments */

/**
 * 2D coordinate with double directives
 * 
 * @author GlaDos
 */
public final class Point implements CSD<Point> {
    /**
     * X coordinate directive (double value)
     */
    private final double x;
    
    /**
     * Y coordinate directive (double value)
     */
    private final double y;
    
    /**
     * sets this coordinate with X and Y, similar as pattern's
     * 
     * @param pattern the coordinate to copy from
     */
    public Point(Point pattern) {
        this.x = pattern.x;
        this.y = pattern.y;
    }
    
    /**
     * simple 2D-coordinate constructor
     * 
     * @param x X directive
     * @param y Y directive
     */
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    /**
     * creates an 'empty' coordinate with zero X and Y
     */
    public Point() {
        this.x = 0;
        this.y = 0;
    }

    public IPoint getIPoint() {
        return new IPoint((int) this.x, (int) this.y);
    }

    /**
     * X directive of a coordinate
     * 
     * @return X directive
     */
    public double getX() {
        return this.x;
    }
    
    /**
     * Y directive of a coordinate
     * 
     * @return Y directive
     */
    public double getY() {
        return this.y;
    }

    public Point changeX(double x) {
        return new Point(x, this.y);
    }

    public Point changeY(double y) {
        return new Point(this.x, y);
    }
    
    /**
     * moves vertex by another 2D-coordinate
     * 
     * @param v offset coordinate
     * @return new moved coordinate
     * @see #move(double, double)
     * @see #moveX(double)
     * @see #moveY(double)
     */
    public Point move(Point v) {
        return new Point(this.x + v.x, this.y + v.y);
    }
    
    /**
     * moves vertex by given X and Y
     * 
     * @param x2 offset X
     * @param y2 offset Y
     * @return new moved coordinate
     * @see #moveX(double)
     * @see #moveY(double)
     */
    public Point move(double x2, double y2) {
        return new Point(this.x + x2, this.y + y2);
    }
    
    /**
     * moves vertex by the given X
     * 
     * @param amount offset X
     * @return new moved coordinate
     * @see #move(double, double)
     * @see #moveY(double)
     */
    public Point moveX(double amount) {
        return new Point(this.x + amount, this.y);
    }
    
    /**
     * moves vertex by the given Y
     * 
     * @param amount offset Y
     * @return new moved coordinate
     * @see #move(double, double)
     * @see #moveX(double)
     */
    public Point moveY(double amount) {
        return new Point(this.x, this.y + amount);
    }
    
    /**
     * multiply each of directives by a double value
     * 
     * @param k factor
     * @return new multiplied coordinate
     * @see #mul(double, double)
     * @see #mulX(double)
     * @see #mulY(double)
     */
    public Point mul(double k) {
        return new Point(this.x * k, this.y * k);
    }
    
    /**
     * multiply each of directives by kX and kY, respectively
     * 
     * @param kX X factor
     * @param kY Y factor
     * @return new multiplied coordinate
     * @see #mul(double)
     * @see #mulX(double)
     * @see #mulY(double)
     */
    public Point mul(double kX, double kY) {
        return new Point(this.x * kX, this.y * kY);
    }
    
    /**
     * multiply X directive by a double value
     * 
     * @param k X factor
     * @return new multiplied coordinate
     * @see #mul(double)
     * @see #mul(double, double)
     * @see #mulY(double)
     */
    public Point mulX(double k) {
        return new Point(this.x * k, this.y);
    }
    
    /**
     * multiply Y directive by a double value
     * 
     * @param k Y factor
     * @return new multiplied coordinate
     * @see #mul(double)
     * @see #mul(double, double)
     * @see #mulX(double)
     */
    public Point mulY(double k) {
        return new Point(this.x, this.y * k);
    }
    
    /**
     * negativate each directive of a vertex
     * 
     * @return new negativated coordinate
     * @see #negX()
     * @see #negY()
     */
    public Point neg() {
        return new Point(-this.x, -this.y);
    }
    
    /**
     * negativate X directive of a vertex
     * 
     * @return new negativated coordinate
     * @see #neg()
     * @see #negY()
     */
    public Point negX() {
        return new Point(-this.x, this.y);
    }
    
    /**
     * negativate Y directive of a vertex
     * 
     * @return new negativated coordinate
     * @see #neg()
     * @see #negX()
     */
    public Point negY() {
        return new Point(this.x, -this.y);
    }
    
    /**
     * normalises vector.
     * If vector is zero-length, it's an exception to be thrown
     * 
     * @return new normalised vector
     * @see #normSilent()
     */
    public Point norm() {
        double d = len();
        if (d == 0) throw new RuntimeException("Attempt to normalize vector of zero length");
        
        return new Point(this.x / d, this.y / d);
    }
    
    /**
     * normalises vector.
     * If vector is zero-length, the result will be an 'empty' vector (0, 0)
     * 
     * @return new normalised vector
     * @see #norm()
     */
    public Point normSilent() {
        double d = len();
        if (d == 0) return new Point();
        
        return new Point(this.x / d, this.y / d);
    }
    
    /**
     * nullifies each directive of a coor
     * 
     * @return new nulllified coordinate
     * @see #nulX()
     * @see #nulY()
     */
    public Point nul() {
        return new Point();
    }
    
    /**
     * nullifies X directive of a coor
     * 
     * @return new coordinate with nullified X
     * @see #nul()
     * @see #nulY()
     */
    public Point nulX() {
        return new Point(0, this.y);
    }
    
    /**
     * nullifies Y directive of a coor
     * 
     * @return new coordinate with nullified Y
     * @see #nul()
     * @see #nulX()
     */
    public Point nulY() {
        return new Point(this.x, 0);
    }
    
    /**
     * replaces each directive by its signum, respectively: 1, if value > 0, -1, if value < 0 and 0 either
     * 
     * @return new cordinate, constructed as a signums of each directive
     */
    public Point sgn() {
        return new Point(signum(this.x), signum(this.y));
    }
    
    /**
     * rotates the point by the given angle
     * 
     * @param angle the angle to rotate by
     * @return new rotated coordinate
     */
    public Point rot(double angle) {
        double s = sin(angle), c = cos(angle);
        return new Point(c * this.x + s * this.y, -s * this.x + c * this.y);
    }
    
    /**
     * rotates the point around the given vertex by the given angle
     * 
     * @param v the point to rotate around
     * @param angle the angle to rotate by
     * @return new rotated coordinate
     * @see #rot(double)
     */
    public Point rot(Point v, double angle) {
        return move(v.neg()).rot(angle).move(v);
    }
    
    /**
     * length of the vector
     * 
     * @return vector length
     */
    public double len() {
        return sqrt(this.x * this.x + this.y * this.y);
    }
    
    /**
     * scalar projection on the given vector
     * 
     * @param l the directive vector to apply projection on
     * @return scalar projection on vector l
     */
    public double projS(Point l) {
        return dotP(this, l) / l.len();
    }
    
    /**
     * vector projection on the given vector
     * 
     * @param l the directive vector to apply projection on
     * @return vector projection on vector l
     */
    public Point proj(Point l) {
        return l.norm().mul(projS(l));
    }
    
    /**
     * calculate distance between two points
     * 
     * @param v1 the first point
     * @param v2 the second point
     * @return the distance betwixt two coordinates
     */
    public static double dis(Point v1, Point v2) {
        double dX = v1.x - v2.x;
        double dY = v1.y - v2.y;
        
        return sqrt(dX * dX + dY * dY);
    }
    
    /**
     * approximately calculate distance between two points.
     * The result is maximum of two catheti.
     * The maximal fault is 2sqrt(a), if v1 and v2 creates a square
     * 
     * @param v1 the first point
     * @param v2 the second point
     * @return the approximate distance betwixt two coordinates
     */
    public static double disApproximate(Point v1, Point v2) {
        return max(v1.x - v2.x, v1.y - v2.y);
    }
    
    /**
     * returns the distance between this and given points
     * 
     * @param v the point to calculate distance by
     * @return the distance between points
     */
    public double disTo(Point v) {
        return dis(this, v);
    }
    
    /**
     * calculates dot product for two vectors
     * 
     * @param vec1 the first vector
     * @param vec2 the second vector
     * @return dot product of two vectors
     */
    public static double dotP(Point vec1, Point vec2) {
        return vec1.x * vec2.x + vec1.y * vec2.y;
    }
    
    /**
     * calculates cross product as a scalar value (cross product length) for two vectors
     * 
     * @param vec1 the first vector
     * @param vec2 the second vector
     * @return cross product of two vectors
     */
    public static double crossP(Point vec1, Point vec2) {
        return vec1.x * vec2.y - vec2.x * vec1.y;
    }
    
    /**
     * calculates angle for two vectors
     * 
     * @param vec1 the first vector
     * @param vec2 the second vector
     * @return angle of two vectors
     */
    public static double angle(Point vec1, Point vec2) {
        return acos(dotP(vec1, vec2) / vec1.len() / vec2.len());
    }
    
    /**
     * the copy of the coordinate
     * 
     * @return the coordinate copy
     */
    @Override
    public Point copy() {
        return this; /* immutable object */
    }
    
    /**
     * Stringifies X and Y directives
     */
    @Override
    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }
    
    /**
     * checks on equality two point
     * 
     * @param p point to check equality with
     * @return boolean decision
     */
    @Override
    public boolean equals(Object p) {
        return (p instanceof Point) && (this.x == ((Point)p).x) && (this.y == ((Point)p).y);
    }
    
    /**
     * returns point hash code
     * 
     * @return hash code of a point
     */
    @Override
    public int hashCode() {
        int hash = 3;
        
        hash = 17 * hash + (int)(Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
        hash = 17 * hash + (int)(Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
        
        return hash;
    }
}
