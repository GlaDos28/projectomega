package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.outWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;

public final class Human implements CSD<Human> {
	private int worldIndex;

	public Human() {
		this.worldIndex = Universe.NO_WORLD;
	}

	private Human(int worldIndex) {
		this.worldIndex = worldIndex;
	}

	//**

	public int getWorldIndex() {
		return this.worldIndex;
	}

	public void setWorldIndex(int worldIndex) {
		this.worldIndex = worldIndex;
	}

	//**

	@Override
	public Human copy() {
		return new Human(this.worldIndex);
	}

	@Override
	public String toString() {
		return "{ world-index: " + this.worldIndex + " }";
	}
}