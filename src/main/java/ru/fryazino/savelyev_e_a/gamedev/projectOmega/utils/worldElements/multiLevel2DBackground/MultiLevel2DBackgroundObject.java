package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.worldElements.multiLevel2DBackground;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Drawable2D;

public final class MultiLevel2DBackgroundObject implements Comparable<MultiLevel2DBackgroundObject> {
	private final MultiLevel2DBackgroundObjectGen generator;
	private float                                 xPos;
	private final int                             height;
	private Drawable2D                            drawable;

	public MultiLevel2DBackgroundObject(MultiLevel2DBackgroundObjectGen generator, float xPos, int height) {
		this.generator = generator;
		this.xPos      = xPos;
		this.height    = height;
		this.drawable  = null;
	}

	MultiLevel2DBackgroundObjectGen getGenerator() {
		return this.generator;
	}

	Drawable2D getDrawable() {
		return this.drawable;
	}

	int getHeight() {
		return this.height;
	}

	float getXPos() {
		return this.xPos;
	}

	void moveXPos(float amount) {
		this.xPos += amount;
	}

	void setDrawable(float scale) {
		this.drawable = this.getGenerator().generate(scale);
	}

	@Override
	public String toString() {
		return "{ generator: " + this.generator.toString() + ", x-pos: " + this.xPos + ", height: " + this.height + ", drawable: " + this.drawable + " }";
	}

	@Override
	public int compareTo(MultiLevel2DBackgroundObject obj) {
		return -Integer.compare(this.height, obj.height);
	}
}
