package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.elements.animatedModel;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Camera2D;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Drawable2D;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.GraphicsAdapter;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.elements.Image;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.IPoint;

public final class Sprite implements Drawable2D {
	private final Image  image;
	private final int    tics;
	private       Sprite next;

	public Sprite(Image image, int tics) {
		this(image, tics, null);
	}

	public Sprite(Image image, int tics, Sprite next) {
		this.image = image;
		this.tics  = tics;
		this.next  = next;
	}

	//**

	public Image getImage() {
		return this.image;
	}

	public int getTics() {
		return this.tics;
	}

	public Sprite getNext() {
		return this.next;
	}

	public Sprite linkNext(Sprite next) {
		this.next = next;
		return next;
	}

	//**

	@Override
	public void draw(GraphicsAdapter graphics, Camera2D camera, IPoint pos, Object... args) {
		this.image.draw(graphics, camera, pos, args);
	}

	@Override
	public String toString() {
		return "{ image: " + this.image.toString() + ", tics: " + this.tics + ", next: " + this.next + " }";
	}
}