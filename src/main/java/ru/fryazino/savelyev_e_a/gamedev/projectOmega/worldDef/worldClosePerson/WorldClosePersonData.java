package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldClosePerson;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.Person;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;

public final class WorldClosePersonData<WD extends WorldData> implements CSD<WorldClosePersonData<WD>> {
	private final Person person;

	public WorldClosePersonData(Person person) {
		this.person = person;
	}

	public Person getPerson() {
		return this.person;
	}

	@Override
	public WorldClosePersonData<WD> copy() {
		return new WorldClosePersonData<>(this.person.copy());
	}

	@Override
	public String toString() {
		return "{ person: " + this.person.toString() + " }";
	}
}