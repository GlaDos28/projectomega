package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.UtilFunctions;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class KeyProcessor<WD extends WorldData> implements CSD<KeyProcessor<WD>> {
	private final KeyProcessType           type;
	private final int                      keyCodeOrChar;
	private final KeyProcessorRunnable<WD> runnable;
	private final Set<Integer>             inputModes;

	public KeyProcessor(KeyProcessType type, int keyCodeOrChar, KeyProcessorRunnable<WD> runnable, Integer... inputModes) {
		this.type          = type;
		this.keyCodeOrChar = keyCodeOrChar;
		this.runnable      = runnable;
		this.inputModes    = new HashSet<>();

		Collections.addAll(this.inputModes, inputModes);
	}

	private KeyProcessor(KeyProcessType type, int keyCodeOrChar, KeyProcessorRunnable<WD> runnable, Set<Integer> inputModes) {
		this.type          = type;
		this.keyCodeOrChar = keyCodeOrChar;
		this.runnable      = runnable;
		this.inputModes    = inputModes;
	}

	public KeyProcessType getType() {
		return this.type;
	}

	public int getKeyCode() {
		return this.keyCodeOrChar;
	}

	public char getKeyChar() {
		return (char) this.keyCodeOrChar;
	}

	public KeyProcessorRunnable<WD> getRunnable() {
		return this.runnable;
	}

	public Set<Integer> getInputModes() {
		return this.inputModes;
	}

	@Override
	public KeyProcessor<WD> copy() {
		return new KeyProcessor<>(this.type, this.keyCodeOrChar, this.runnable, UtilFunctions.copyIntSet(this.inputModes));
	}

	@Override
	public String toString() {
		return "{ process-type: " + this.type.toString() + ", key-code-or-char: " + this.keyCodeOrChar + ", runnable: " + this.runnable + ", " + UtilFunctions.stringifyCollection("input-modes", this.inputModes) + " }";
	}
}