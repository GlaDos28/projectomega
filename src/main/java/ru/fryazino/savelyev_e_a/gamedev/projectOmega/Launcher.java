package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.game.GameConfig;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.game.GameFacade;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.game.GameManager;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.GraphicsConfig;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.impl.custom.frustratedWorld.FrustratedWorld;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.impl.custom.frustratedWorld.FrustratedWorldInitWorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.impl.custom.testWorld.TestWorld;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.impl.custom.testWorld.TestWorldInitWorldData;
import javafx.util.Pair;

public final class Launcher {
	public static void main(String[] args) {
		/*GameFacade game = GameManager.createGameInstance(new GameConfig()
			.setWorldDefs(TestWorld.TEST_WORLD_DEF)
			.setWorlds(new Pair<>(TestWorld.TEST_WORLD_DEF, new TestWorldInitWorldData(100, 100)))
			.setGraphicsConfig(new GraphicsConfig(1000, 1000))
			.setGameCycleMaxUpdateTime(10L)
			.build()
		);

		game.start();
		game.enterWorld(0);*/

		GameFacade game = GameManager.createGameInstance(new GameConfig()
			.setWorldDefs(FrustratedWorld.FRUSTRATED_WORLD_DEF)
			.setWorlds(new Pair<>(FrustratedWorld.FRUSTRATED_WORLD_DEF, new FrustratedWorldInitWorldData()))
			.setGraphicsConfig(new GraphicsConfig(1900, 1000))
			.setGameCycleUrMult(4)
			.setGameCycleMaxUpdateTime(10L)
			.build()
		);

		game.start();
		game.enterWorld(0);
	}
}