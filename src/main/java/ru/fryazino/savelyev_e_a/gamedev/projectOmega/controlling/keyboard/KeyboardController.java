package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public final class KeyboardController extends KeyAdapter {
	private final KeyboardState state;

	public KeyboardController() {
		this.state = new KeyboardState();
	}

	public KeyboardState getState() {
		return this.state;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		this.state.addTypedKeyAction(e.getKeyChar());
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (!this.state.isKeyPressed(e.getKeyCode())) {
			this.state.setKeyPressed(e.getKeyCode());
			this.state.addPressedKeyAction(e.getKeyCode());
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		this.state.setKeyReleased(e.getKeyCode());
		this.state.addReleasedKeyAction(e.getKeyCode());
	}

	//**

	@Override
	public String toString() {
		return
			"{ state: " + this.state.toString() + " }";
	}
}