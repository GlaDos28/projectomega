package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;

public final class GraphicsConfig implements CSD<GraphicsConfig> {
	private final int scrWidth, scrHeight;

	public GraphicsConfig(int scrWidth, int scrHeight) {
		this.scrWidth  = scrWidth;
		this.scrHeight = scrHeight;
	}

	//**

	public int getScrWidth() {
		return this.scrWidth;
	}

	public int getScrHeight() {
		return this.scrHeight;
	}

	//**

	@Override
	public GraphicsConfig copy() {
		return this; /* immutable object */
	}

	@Override
	public String toString() {
		return "{ scr-width: " + this.scrWidth + ", scr-height: " + this.scrHeight + " }";
	}
}