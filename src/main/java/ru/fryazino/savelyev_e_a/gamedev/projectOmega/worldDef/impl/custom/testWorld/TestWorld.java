package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.impl.custom.testWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.GraphicsAdapter;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.Person;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.WorldDef;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.IPoint;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.Point;

import java.awt.*;

public final class TestWorld {
	private TestWorld() {}

	public static final WorldDef<TestWorldData> TEST_WORLD_DEF = new WorldDef<>(
		(data) -> {
			TestWorldInitWorldData testWorldInitData = (TestWorldInitWorldData) data;
			return new TestWorldData(testWorldInitData.getFieldSizeN(), testWorldInitData.getFieldSizeM(), new IPoint(0, 0));
		},
		(data) -> new Person(data.getHuman()),
		(data) -> {
			data.processKeyboardKeysStandard(0);
		},
		(data) -> {
			int w = data.getGraphicsData().getScrWidth();
			int h = data.getGraphicsData().getScrHeight();
			int n = data.getWorldData().getN();
			int m = data.getWorldData().getM();
			IPoint personPos = data.getWorldData().getPersonPos();

			GraphicsAdapter g = data.getGraphicsData().getGraphics();

			//** clearing the screen

			g.setColor(Color.BLACK);
			g.drawRect(0, 0, data.getGraphicsData().getScrWidth(), data.getGraphicsData().getScrHeight());

			//** filling the field

			g.setColor(new Color(50, 10, 40));
			Point offset = new Point(2, 2);
			Point size   = new Point((float) (w - offset.getX()) / n - offset.getX(), (float) (h - offset.getY()) / m - offset.getY());
			Point p      = new Point(offset.getX(), offset.getY());

			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {
					g.drawRect(p.getX(), p.getY(), size.getX(), size.getY());
					p = new Point(p.getX() + size.getX() + offset.getX(), p.getY());
				}

				p = new Point(offset.getX(), p.getY() + size.getY() + offset.getY());
			}

			//** putting the person

			g.setColor(Color.CYAN);
			g.drawRect((personPos.getX() * (size.getX() + offset.getX()) + offset.getX()), (personPos.getY() * (size.getY() + offset.getY()) + offset.getY()), size.getX(), size.getY());
		},
		(data) -> {},
		(data) -> {},
		(data) -> {},
		(data) -> {}
	);
}