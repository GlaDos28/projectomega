package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldInitWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;

@FunctionalInterface
public interface IWorldInitWorld<WD extends WorldData> {
	WD initWorld(WorldInitWorldData<WD> data);
}