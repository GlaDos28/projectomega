package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.game;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.GraphicsAdapter;

public final class GraphicsTransmitData {
	private final GraphicsAdapter graphics;
	private final int scrWidth;
	private final int scrHeight;

	GraphicsTransmitData(GraphicsAdapter graphics, int scrWidth, int scrHeight) {
		this.graphics  = graphics;
		this.scrWidth  = scrWidth;
		this.scrHeight = scrHeight;
	}

	//**

	public GraphicsAdapter getGraphics() {
		return this.graphics;
	}

	public int getScrWidth() {
		return this.scrWidth;
	}

	public int getScrHeight() {
		return this.scrHeight;
	}

	//**

	@Override
	public String toString() {
		return "{ graphics-adapter: " + this.graphics.toString() + ", scr-width: " + this.scrWidth + ", scr-height: " + this.scrHeight + " }";
	}
}