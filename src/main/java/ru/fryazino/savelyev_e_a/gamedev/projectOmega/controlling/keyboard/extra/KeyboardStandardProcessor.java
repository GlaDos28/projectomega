package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.KeyboardState;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldExecTic.WorldExecTicData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.UtilFunctions;

import java.util.*;

public final class KeyboardStandardProcessor<WD extends WorldData> implements CSD<KeyboardStandardProcessor<WD>> {
	private final ArrayList<ArrayList<Map<Integer, KeyProcessor<WD>>>> inputModes;

	public KeyboardStandardProcessor() {
		this.inputModes = new ArrayList<>();
	}

	private void addNewMode() {
		this.inputModes.add(new ArrayList<>());
		this.inputModes.get(this.inputModes.size() - 1).add(new HashMap<>()); /* CONTINUOUS */
		this.inputModes.get(this.inputModes.size() - 1).add(new HashMap<>()); /* ONE_TIME   */
		this.inputModes.get(this.inputModes.size() - 1).add(new HashMap<>()); /* TYPING     */
	}

	public void addKeyProcessor(KeyProcessor<WD> keyProcessor) {
		for (int inputMode : keyProcessor.getInputModes()) {
			if (inputMode == -1) {
				for (ArrayList<Map<Integer, KeyProcessor<WD>>> mode : this.inputModes) {
					mode.get(keyProcessor.getType().ordinal()).put(keyProcessor.getKeyCode(), keyProcessor);
				}
			} else {
				while (this.inputModes.size() <= inputMode) {
					this.addNewMode();
				}

				this.inputModes.get(inputMode).get(keyProcessor.getType().ordinal()).put(keyProcessor.getKeyCode(), keyProcessor);
			}
		}
	}

	public void processInput(KeyboardState keyboardState, int mode, WorldExecTicData<WD> worldExecTicData) {
		Queue<Integer> pressedKeyActions = keyboardState.getPressedKeyActions();

		for (int key : pressedKeyActions) {
			KeyProcessor<WD> continuousProcessor = this.inputModes.get(mode).get(KeyProcessType.CONTINUOUS.ordinal()).get(key);
			KeyProcessor<WD> oneTimeProcessor    = this.inputModes.get(mode).get(KeyProcessType.ONE_TIME.ordinal()).get(key);

			if (continuousProcessor != null) {
				continuousProcessor.getRunnable().processKey(key, worldExecTicData);
			}

			if (oneTimeProcessor != null) {
				oneTimeProcessor.getRunnable().processKey(key, worldExecTicData);
			}
		}

		for (int key : keyboardState.getPressedKeys()) {
			if (!pressedKeyActions.contains(key)) {
				KeyProcessor<WD> continuousProcessor = this.inputModes.get(mode).get(KeyProcessType.CONTINUOUS.ordinal()).get(key);

				if (continuousProcessor != null) {
					continuousProcessor.getRunnable().processKey(key, worldExecTicData);
				}
			}
		}

		Queue<Character> typedKeyActions = keyboardState.getTypedKeyActions();

		for (char key : typedKeyActions) {
			KeyProcessor<WD> typingProcessor = this.inputModes.get(mode).get(KeyProcessType.TYPING.ordinal()).get((int) key);

			if (typingProcessor != null) {
				typingProcessor.getRunnable().processKey(key, worldExecTicData);
			}
		}
	}

	@Override
	public KeyboardStandardProcessor<WD> copy() {
		return new KeyboardStandardProcessor<>();
	}

	@Override
	public String toString() {
		return "{ " + UtilFunctions.stringifyArrayList("input-modes", this.inputModes) + " }";
	}
}