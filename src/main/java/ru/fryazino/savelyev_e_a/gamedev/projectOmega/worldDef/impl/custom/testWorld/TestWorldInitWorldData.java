package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.impl.custom.testWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra.KeyProcessType;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra.KeyProcessor;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra.KeyboardKeyStandardProcessing;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldInitWorld.WorldInitWorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.IPoint;

import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.Set;

public final class TestWorldInitWorldData extends WorldInitWorldData<TestWorldData> implements KeyboardKeyStandardProcessing<TestWorldData> {
	private final int fieldSizeN, fieldSizeM;

	public TestWorldInitWorldData(int fieldSizeN, int fieldSizeM) {
		this.fieldSizeN = fieldSizeN;
		this.fieldSizeM = fieldSizeM;
	}

	//**

	int getFieldSizeN() {
		return this.fieldSizeN;
	}

	int getFieldSizeM() {
		return this.fieldSizeM;
	}

	//**

	@Override
	public Set<KeyProcessor<TestWorldData>> getKeyProcessors() {
		Set<KeyProcessor<TestWorldData>> res = new HashSet<>();

		res.add(new KeyProcessor<>(KeyProcessType.CONTINUOUS, KeyEvent.VK_W, (key, data) -> {
			data.getWorldData().setPersonPos(new IPoint(data.getWorldData().getPersonPos().getX(), Math.max(0, data.getWorldData().getPersonPos().getY() - 1)));
		}, 0));

		res.add(new KeyProcessor<>(KeyProcessType.CONTINUOUS, KeyEvent.VK_S, (key, data) -> {
			data.getWorldData().setPersonPos(new IPoint(data.getWorldData().getPersonPos().getX(), Math.min(data.getWorldData().getN() - 1, data.getWorldData().getPersonPos().getY() + 1)));
		}, 0));

		res.add(new KeyProcessor<>(KeyProcessType.CONTINUOUS, KeyEvent.VK_D, (key, data) -> {
			data.getWorldData().setPersonPos(new IPoint(Math.min(data.getWorldData().getM() - 1, data.getWorldData().getPersonPos().getX() + 1), data.getWorldData().getPersonPos().getY()));
		}, 0));

		res.add(new KeyProcessor<>(KeyProcessType.CONTINUOUS, KeyEvent.VK_A, (key, data) -> {
			data.getWorldData().setPersonPos(new IPoint(Math.max(0, data.getWorldData().getPersonPos().getX() - 1), data.getWorldData().getPersonPos().getY()));
		}, 0));

		return res;
	}

	@Override
	public TestWorldInitWorldData copy() {
		return new TestWorldInitWorldData(this.fieldSizeN, this.fieldSizeM);
	}

	@Override
	public String toString() {
		return "{ field-size-N: " + this.fieldSizeN + ", field-size-M: " + this.fieldSizeM + " }";
	}
}