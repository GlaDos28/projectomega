package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.worldElements.multiLevel2DBackground;

import java.util.ArrayList;

@FunctionalInterface
public interface MultiLevel2DBackgroundGenFunc {
	ArrayList<MultiLevel2DBackgroundObject> generateBackground(int l, int r, int maxHeight);
}
