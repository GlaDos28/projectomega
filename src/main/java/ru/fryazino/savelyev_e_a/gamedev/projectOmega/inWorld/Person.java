package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.outWorld.Human;

public class Person implements CSD<Person> {
	private final Human humanRef;

	public Person(Human humanRef) {
		this.humanRef = humanRef;
	}

	//**

	public Human getHumanRef() {
		return this.humanRef;
	}

	//**

	@Override
	public Person copy() {
		return new Person(this.humanRef);
	}

	@Override
	public String toString() {
		return "{ human-ref: " + this.humanRef + " }";
	}
}