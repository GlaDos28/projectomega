package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.worldElements.multiLevel2DBackground;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Drawable2D;

@FunctionalInterface
public interface MultiLevel2DBackgroundObjectGen<D extends Drawable2D> {
	D generate(float scale);
}
