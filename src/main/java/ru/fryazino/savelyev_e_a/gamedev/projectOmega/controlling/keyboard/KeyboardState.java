package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.UtilFunctions;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public final class KeyboardState implements CSD<KeyboardState> {
	private final Set<Integer>     pressedKeys;
	private final Set<Integer>     releasedKeys;
	private final Queue<Integer>   pressedKeyActions;
	private final Queue<Integer>   releasedKeyActions;
	private final Queue<Character> typedKeyActions;

	public KeyboardState() {
		this(new HashSet<>(), new HashSet<>(), new LinkedList<>(), new LinkedList<>(), new LinkedList<>());
	}

	public KeyboardState(Set<Integer> pressedKeys, Set<Integer> releasedKeys, Queue<Integer> pressedKeyActions, Queue<Integer> releasedKeyActions, Queue<Character> typedKeyActions) {
		this.pressedKeys        = pressedKeys;
		this.releasedKeys       = releasedKeys;
		this.pressedKeyActions  = pressedKeyActions;
		this.releasedKeyActions = releasedKeyActions;
		this.typedKeyActions    = typedKeyActions;
	}

	//**

	public boolean isKeyPressed(int keyCode) {
		return this.pressedKeys.contains(keyCode);
	}

	public boolean isKeyClicked(int keyCode) {
		return this.pressedKeyActions.contains(keyCode);
	}

	public boolean isKeyInPressing(int keyCode) {
		return this.pressedKeys.contains(keyCode) && !this.pressedKeyActions.contains(keyCode);
	}

	public boolean isKeyReleased(int keyCode) {
		return this.releasedKeys.contains(keyCode);
	}

	public void setKeyPressed(int keyCode) {
		this.pressedKeys.add(keyCode);
	}

	public void setKeyReleased(int keyCode) {
		this.pressedKeys.remove(keyCode);
	}

	public void addPressedKeyAction(int keyCode) {
		this.pressedKeyActions.add(keyCode);
	}

	public void addReleasedKeyAction(int keyCode) {
		this.releasedKeyActions.add(keyCode);
	}

	public void addTypedKeyAction(char keyChar) {
		this.typedKeyActions.add(keyChar);
	}

	public Queue<Integer> getPressedKeyActions() {
		return this.pressedKeyActions;
	}

	public Queue<Integer> getReleasedKeyActions() {
		return this.releasedKeyActions;
	}

	public Queue<Character> getTypedKeyActions() {
		return this.typedKeyActions;
	}

	public Set<Integer> getPressedKeys() {
		return this.pressedKeys;
	}

	public Set<Integer> getReleasedKeys() {
		return this.releasedKeys;
	}

	public void update() {
		this.pressedKeyActions.clear();
		this.releasedKeyActions.clear();
		this.typedKeyActions.clear();
	}

	//**

	@Override
	public KeyboardState copy() {
		return new KeyboardState(UtilFunctions.copyIntSet(this.pressedKeys), UtilFunctions.copyIntSet(this.releasedKeys), UtilFunctions.copyIntQueue(this.pressedKeyActions), UtilFunctions.copyIntQueue(this.releasedKeyActions), UtilFunctions.copyCharQueue(this.typedKeyActions));
	}

	@Override
	public String toString() {
		return "{ " +
			UtilFunctions.stringifyCollection("pressed-keys",         this.pressedKeys)        + ", " +
			UtilFunctions.stringifyCollection("pressed-keys",         this.pressedKeys)        + ", " +
			UtilFunctions.stringifyCollection("pressed-key-actions",  this.pressedKeyActions)  + ", " +
			UtilFunctions.stringifyCollection("released-key-actions", this.releasedKeyActions) + ", " +
			UtilFunctions.stringifyCollection("typed-key-actions",    this.typedKeyActions)    + " }";
	}
}