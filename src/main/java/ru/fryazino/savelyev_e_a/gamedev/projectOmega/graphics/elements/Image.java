package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.elements;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Camera2D;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Drawable2D;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.GraphicsAdapter;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.UtilFunctions;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.IPoint;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

public final class Image implements Drawable2D {
	private java.awt.Image rawImage;
	private final int w, h;
	private final boolean centX, centY;
	private final boolean flipX, flipY;

	public Image(String imagePath) {
		this(UtilFunctions.loadImage(imagePath));
	}

	public Image(String imagePath, int w, int h) {
		this(UtilFunctions.loadImage(imagePath), w, h, false, false);
	}

	public Image(String imagePath, int w, int h, boolean centX, boolean centY) {
		this(UtilFunctions.loadImage(imagePath), w, h, centX, centY);
	}

	public Image(java.awt.Image rawImage, int w, int h) {
		this(rawImage, w, h, false, false);
	}

	public Image(String imagePath, float scale) {
		this(UtilFunctions.loadImage(imagePath), scale, false, false);
	}

	public Image(String imagePath, float scale, boolean centX, boolean centY) {
		this(UtilFunctions.loadImage(imagePath), scale, centX, centY);
	}

	public Image(String imagePath, float scale, boolean centX, boolean centY, boolean flipX, boolean flipY) {
		this(UtilFunctions.loadImage(imagePath), scale, centX, centY, flipX, flipY);
	}

	public Image(java.awt.Image rawImage, float scale) {
		this(rawImage, scale, false, false);
	}

	public Image(java.awt.Image rawImage, int w, int h, boolean centX, boolean centY) {
		this(rawImage, w, h, centX, centY, false, false);
	}

	public Image(java.awt.Image rawImage, float scale, boolean centX, boolean centY) {
		this(rawImage, scale, centX, centY, false, false);
	}

	public Image(java.awt.Image rawImage) {
		this.rawImage = rawImage;
		this.w        = this.rawImage.getWidth(null);
		this.h        = this.rawImage.getHeight(null);
		this.centX    = false;
		this.centY    = false;
		this.flipX    = false;
		this.flipY    = false;

		this.flipImage();
	}

	public Image(java.awt.Image rawImage, int w, int h, boolean centX, boolean centY, boolean flipX, boolean flipY) {
		this.rawImage = rawImage;
		this.w        = w;
		this.h        = h;
		this.centX    = centX;
		this.centY    = centY;
		this.flipX    = flipX;
		this.flipY    = flipY;

		this.flipImage();
	}

	public Image(java.awt.Image rawImage, float scale, boolean centX, boolean centY, boolean flipX, boolean flipY) {
		this.rawImage = rawImage;
		this.w        = (int) (this.rawImage.getWidth(null) * scale);
		this.h        = (int) (this.rawImage.getHeight(null) * scale);
		this.centX    = centX;
		this.centY    = centY;
		this.flipX    = flipX;
		this.flipY    = flipY;

		this.flipImage();
	}

	private void flipImage() {
		if (this.flipX || this.flipY) {
			AffineTransform tx = AffineTransform.getScaleInstance(this.flipX ? -1 : 1, this.flipY ? -1 : 1);
			tx.translate(this.flipX ? -this.rawImage.getWidth(null) : 0, this.flipY ? -this.rawImage.getHeight(null) : 0);

			AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);

			this.rawImage = op.filter((BufferedImage) this.rawImage, null);
		}
	}

	//**

	public java.awt.Image getRawImage() {
		return this.rawImage;
	}

	public int getWidth() {
		return this.w;
	}

	public int getHeight() {
		return this.h;
	}

	public Image scale(float k) {
		return new Image(this.rawImage, k);
	}

	public boolean isCentX() {
		return this.centX;
	}

	public boolean isCentY() {
		return this.centY;
	}

	public boolean isFlipX() {
		return this.flipX;
	}

	public boolean isFlipY() {
		return this.flipY;
	}

	//**

	@Override
	public void draw(GraphicsAdapter graphics, Camera2D camera, IPoint pos, Object... args) { /* args[0] - flipX, args[1] - flipY */
		IPoint p = pos.dif(camera.getPos()).sum(graphics.getConfig().getScrWidth() / 2, graphics.getConfig().getScrHeight() / 2);

		if (this.centX) {
			p = p.difX(this.w / 2);
		}

		if (this.centY) {
			p = p.difY(this.h / 2);
		}

		boolean flipX = false;
		boolean flipY = false;

		if (args.length > 0 && args[0] != null && args[0].equals(true)) {
			flipX = true;
		}

		if (args.length > 1 && args[0] != null && args[1].equals(true)) {
			flipY = true;
		}

		graphics.drawRawImage(this.rawImage, p, this.w, this.h, flipX, flipY);
	}

	@Override
	public String toString() {
		return "{ raw-image: " + this.rawImage.toString() + ", width: " + this.w + ", height: " + this.h + ", cent-x: " + this.centX + ", cent-y: " + this.centY + " }";
	}
}