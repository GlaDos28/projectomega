package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;

public abstract class WorldData implements CSD<WorldData> {
	private Person person;

	protected WorldData() {}

	public void setPerson(Person person) {
		this.person = person;
	}

	public <P extends Person> P getPerson() { /* careful with casting */
		return (P) this.person;
	}

	@Override
	public abstract String toString();

	@Override
	public abstract WorldData copy();
}