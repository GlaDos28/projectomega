package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.elements.animatedModel;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Camera2D;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Drawable2D;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.GraphicsAdapter;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.UtilFunctions;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.IPoint;

import java.util.ArrayList;
import java.util.Collections;

public final class AnimatedModel implements Drawable2D {
	private final ArrayList<Sprite> stateSprites;
	private int ticCounter;
	private Sprite curSprite;
	private int curState;

	public AnimatedModel() {
		this.stateSprites = new ArrayList<>();
		this.ticCounter   = 0;
		this.curSprite    = null;
		this.curState     = -1;
	}

	public AnimatedModel(int firstStateInd, Sprite... stateFirstSprites) {
		this.ticCounter = 0;
		this.curState   = firstStateInd;

		this.stateSprites = new ArrayList<>();
		Collections.addAll(this.stateSprites, stateFirstSprites);

		this.curSprite = this.stateSprites.get(firstStateInd);
	}

	//**

	public int addState(Sprite stateFirstSprite) {
		this.stateSprites.add(stateFirstSprite);
		return this.stateSprites.size() - 1;
	}

	public Sprite getStateFirstSprite(int stateNum) {
		return this.stateSprites.get(stateNum);
	}

	public int getCurStateNum() {
		return this.curState;
	}

	//**

	public void tic() {
		this.ticCounter++;

		if (this.ticCounter >= this.curSprite.getTics()) {
			this.ticCounter = 0;
			this.curSprite = this.curSprite.getNext();
		}
	}

	public void setState(int stateNum) {
		this.ticCounter = 0;
		this.curState   = stateNum;
		this.curSprite  = this.stateSprites.get(stateNum);
	}

	//**

	@Override
	public void draw(GraphicsAdapter graphics, Camera2D camera, IPoint pos, Object... args) {
		this.curSprite.draw(graphics, camera, pos, args);
	}

	@Override
	public String toString() {
		return "{ " + UtilFunctions.stringifyArrayList("state-sprites", this.stateSprites) + ", tic-counter: " + this.ticCounter + ", cur-sprite: " + this.curSprite.toString() + ", cur-state: " + this.curState + " }";
	}
}