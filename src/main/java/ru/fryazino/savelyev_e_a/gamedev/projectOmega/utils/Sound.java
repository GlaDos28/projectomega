package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public final class Sound {
	private final boolean released;
	private Clip         clip;
	private FloatControl volumeC;
	private boolean      playing;

	//**

	private class Listener implements LineListener {
		public void update(LineEvent ev) {
			if (ev.getType() == LineEvent.Type.STOP) {
				Sound.this.playing = false;

				synchronized(Sound.this.clip) {
					Sound.this.clip.notify();
				}
			}
		}
	}

	//**
	
	public Sound(File f) {
		this.playing  = false;

		try {
			AudioInputStream stream = AudioSystem.getAudioInputStream(f);

			this.clip = AudioSystem.getClip();
			this.clip.open(stream);
			this.clip.addLineListener(new Listener());

			this.volumeC = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
		} catch (IOException | UnsupportedAudioFileException | LineUnavailableException ex) {
			ex.printStackTrace();
			this.released = false;

			return;
		}

		this.released = true;
	}

	public boolean isReleased() {
		return this.released;
	}
	
	public boolean isPlaying() {
		return playing;
	}

	public void play(boolean breakOld) {
		if (this.released) {
			if (breakOld) {
				this.clip.stop();
				this.clip.setFramePosition(0);
				this.clip.start();

				this.playing = true;
			} else if (!this.playing) {
				this.clip.setFramePosition(0);
				this.clip.start();

				this.playing = true;
			}
		}
	}
	
	public void play() {
		this.play(true);
	}
	
	public void stop() {
		if (this.playing) {
			this.clip.stop();
		}
	}

	public void setVolume(float vol) {
		if (vol < 0) {
			vol = 0;
		}

		if (vol > 1) {
			vol = 1;
		}

		float min = this.volumeC.getMinimum();
		float max = this.volumeC.getMaximum();

		this.volumeC.setValue((max - min) * vol + min);
	}
	
	public float getVolume() {
		float v   = this.volumeC.getValue();
		float min = this.volumeC.getMinimum();
		float max = this.volumeC.getMaximum();

		return (v - min) / (max - min);
	}

	public void join() {
		if (!this.released) {
			return;
		}

		synchronized(this.clip) {
			try {
				while (this.playing) this.clip.wait();
			} catch (InterruptedException ex) {}
		}
	}

	public static Sound playSound(String filePath) {
		return Sound.playSound(filePath, 1);
	}

	public static Sound playSound(String filePath, float volume) {
		File  f = new File(filePath);
		Sound s = new Sound(f);

		s.setVolume(volume);
		s.play();

		return s;
	}
}