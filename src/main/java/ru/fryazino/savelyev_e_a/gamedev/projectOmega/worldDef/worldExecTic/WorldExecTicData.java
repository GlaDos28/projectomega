package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldExecTic;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.Controller;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.Person;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.World;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;

public final class WorldExecTicData<WD extends WorldData> implements CSD<WorldExecTicData<WD>> {
	private final World<WD>  world;
	private final Controller controller; /* link to the controller */

	public WorldExecTicData(World<WD> world, Controller controller) {
		this.world      = world;
		this.controller = controller;
	}

	public WD getWorldData() {
		return this.world.getWorldData();
	}

	public <P extends Person> P getPerson() { /* careful with casting */
		return (P) this.world.getPerson();
	}

	public Controller getController() {
		return this.controller;
	}

	public void processKeyboardKeysStandard(int mode) {
		this.world.getExtraData().getKeyboardStandardProcessor().processInput(this.controller.getKeyboardController().getState().copy(), mode, this); /* without copy() can be produced concurrent exceptions and so on */
	}

	@Override
	public WorldExecTicData<WD> copy() {
		return new WorldExecTicData<>(this.world.copy(), this.controller);
	}

	@Override
	public String toString() {
		return "{ world: " + this.world.toString() + ", linked-controller: " + this.controller.toString() + " }";
	}
}