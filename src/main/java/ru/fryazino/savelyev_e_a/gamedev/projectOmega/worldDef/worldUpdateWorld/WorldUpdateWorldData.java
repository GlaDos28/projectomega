package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldUpdateWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;

public final class WorldUpdateWorldData<WD extends WorldData> implements CSD<WorldUpdateWorldData<WD>> {
	private final WD   worldData;
	private final long timePassed;

	public WorldUpdateWorldData(WD worldData, long timePassed) {
		this.worldData  = worldData;
		this.timePassed = timePassed;
	}

	public WD getWorldData() {
		return this.worldData;
	}

	public long getTimePassed() {
		return this.timePassed;
	}

	@Override
	public WorldUpdateWorldData<WD> copy() {
		return new WorldUpdateWorldData<>((WD) worldData.copy(), this.timePassed);
	}

	@Override
	public String toString() {
		return "{ outWorld-data: " + this.worldData.toString() + ", time-passed: " + this.timePassed + " }";
	}
}