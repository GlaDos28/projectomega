package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.impl.custom.frustratedWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra.KeyProcessType;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra.KeyProcessor;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra.KeyboardKeyStandardProcessing;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldInitWorld.WorldInitWorldData;

import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.Set;

public final class FrustratedWorldInitWorldData extends WorldInitWorldData<FrustratedWorldData> implements KeyboardKeyStandardProcessing<FrustratedWorldData> {
	public FrustratedWorldInitWorldData() {}

	//**

	@Override
	public Set<KeyProcessor<FrustratedWorldData>> getKeyProcessors() {
		Set<KeyProcessor<FrustratedWorldData>> res = new HashSet<>();

		res.add(new KeyProcessor<>(KeyProcessType.CONTINUOUS, KeyEvent.VK_D, (key, data) -> {
			FrustratedWorldPerson person = data.getPerson();

			if (person.getAnimatedModel().getCurStateNum() != 1) {
				person.getAnimatedModel().setState(1);
			}

			person.setLookingLeft(false);
			data.getWorldData().movePerson(3);
		}, 0));

		res.add(new KeyProcessor<>(KeyProcessType.CONTINUOUS, KeyEvent.VK_A, (key, data) -> {
			FrustratedWorldPerson person = data.getPerson();

			if (person.getAnimatedModel().getCurStateNum() != 1) {
				person.getAnimatedModel().setState(1);
			}

			person.setLookingLeft(true);
			data.getWorldData().movePerson(-3);
		}, 0));

		res.add(new KeyProcessor<>(KeyProcessType.ONE_TIME, KeyEvent.VK_SPACE, (key, data) -> {
			FrustratedWorldPerson person = data.getPerson();

			person.setSpeed(person.getSpeed().moveY(10));
		}, 0));

		return res;
	}

	@Override
	public FrustratedWorldInitWorldData copy() {
		return new FrustratedWorldInitWorldData();
	}

	@Override
	public String toString() {
		return "{ frustrated-world-init-world-data }";
	}
}