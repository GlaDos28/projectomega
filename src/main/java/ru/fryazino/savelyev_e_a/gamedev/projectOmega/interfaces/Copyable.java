package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces;

public interface Copyable<T> {
	T copy();
}
