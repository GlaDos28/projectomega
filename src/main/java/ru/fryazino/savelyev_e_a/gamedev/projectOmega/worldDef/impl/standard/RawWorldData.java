package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.impl.standard;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.UtilFunctions;

import java.util.ArrayList;

public final class RawWorldData<T extends CSD<T>> extends WorldData {
	private final ArrayList<T> rawObjects;

	public RawWorldData() {
		this.rawObjects = new ArrayList<>();
	}

	private RawWorldData(ArrayList<T> rawObjects) {
		this.rawObjects = rawObjects;
	}

	@Override
	public String toString() {
		return "{ raw-outWorld-data | " + UtilFunctions.stringifyArrayList("objects", this.rawObjects) + " }";
	}

	@Override
	public RawWorldData copy() {
		return new RawWorldData(UtilFunctions.copyArrayList(this.rawObjects));
	}
}