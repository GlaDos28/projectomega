package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.impl.custom.frustratedWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.Point;

public final class WorldGravity implements CSD<WorldGravity> {
	public static final Point        DEFAULT_SPEED_COUNTERACTION_FACTOR = new Point(0.9, 0.99);
	public static final float        DEFAULT_ZERO_SPEED_THRESHOLD       = 0.1f;
	public static final float        DEFAULT_FALL_ACCELERATION          = 0.4f;
	public static final WorldGravity DEFAULT_GRAVITY                    = new WorldGravity(DEFAULT_SPEED_COUNTERACTION_FACTOR, DEFAULT_ZERO_SPEED_THRESHOLD, DEFAULT_FALL_ACCELERATION);

	private final Point speedCounteractionFactor; /* actually 1 corresponds to no counteraction and 0 to full counteraction (drop to zero speed every time) */
	private final float zeroSpeedThreshold;
	private final float fallAcceleration;

	public WorldGravity(Point speedCounteractionFactor, float zeroSpeedThreshold, float fallAcceleration) {
		this.speedCounteractionFactor = speedCounteractionFactor;
		this.zeroSpeedThreshold       = zeroSpeedThreshold;
		this.fallAcceleration         = fallAcceleration;
	}

	Point getSpeedCounteractionFactor() {
		return this.speedCounteractionFactor;
	}

	float getZeroSpeedThreshold() {
		return this.zeroSpeedThreshold;
	}

	float getFallAcceleration() {
		return this.fallAcceleration;
	}

	@Override
	public WorldGravity copy() {
		return this; /* immutable object */
	}

	@Override
	public String toString() {
		return "{ speed-counteraction-factor: " + this.speedCounteractionFactor.toString() + ", zero-speed-threshold : " + this.zeroSpeedThreshold + ", fall-acceleration: " + this.fallAcceleration + " }";
	}
}