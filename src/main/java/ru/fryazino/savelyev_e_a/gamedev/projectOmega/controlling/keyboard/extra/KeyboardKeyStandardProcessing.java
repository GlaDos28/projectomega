package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;

import java.util.Set;

@FunctionalInterface
public interface KeyboardKeyStandardProcessing<WD extends WorldData> {
	Set<KeyProcessor<WD>> getKeyProcessors();
}