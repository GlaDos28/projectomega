package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils;

public final class Container<T> {
	private T object;

	public Container() {
		this(null);
	}

	public Container(T object) {
		this.object = object;
	}

	public T get() {
		return this.object;
	}

	public void set(T newObject) {
		this.object = newObject;
	}

	@Override
	public String toString() {
		return this.object.toString();
	}
}
