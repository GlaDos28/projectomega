package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.controlling.keyboard.extra.KeyboardStandardProcessor;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;

public class WorldExtraData<WD extends WorldData> implements CSD<WorldExtraData<WD>> {
	private final KeyboardStandardProcessor<WD> keyboardStandardProcessor;

	public WorldExtraData(KeyboardStandardProcessor<WD> keyboardStandardProcessor) {
		this.keyboardStandardProcessor = keyboardStandardProcessor;
	}

	public KeyboardStandardProcessor<WD> getKeyboardStandardProcessor() {
		return this.keyboardStandardProcessor;
	}

	@Override
	public WorldExtraData<WD> copy() {
		return new WorldExtraData<>(this.keyboardStandardProcessor.copy());
	}

	@Override
	public String toString() {
		return "{ keyboard-standard-processor: " + this.keyboardStandardProcessor.toString() + " }";
	}
}