package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldRender;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;

@FunctionalInterface
public interface
IWorldRender<WD extends WorldData> {
	void render(WorldRenderData<WD> data);
}