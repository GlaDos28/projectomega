package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.impl.custom.frustratedWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.elements.Image;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.IPoint;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.worldElements.multiLevel2DBackground.MultiLevel2DBackground;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.worldElements.multiLevel2DBackground.MultiLevel2DBackgroundDef;

public final class FrustratedWorldData extends WorldData {
	private static final IPoint BACKGROUND_POS = new IPoint(0, 200);

	private final MultiLevel2DBackground background;

	public FrustratedWorldData(Image backgroundImage, Image backgroundObjectImage) {
		super();
		this.background = new MultiLevel2DBackground(new MultiLevel2DBackgroundDef(backgroundImage, 2000, 1000, 300, 0.1f, 0.01, backgroundObjectImage::scale), BACKGROUND_POS);
	}

	//**

	public MultiLevel2DBackground getBackground() {
		return this.background;
	}

	void movePerson(float amount) {
		FrustratedWorldPerson person = super.getPerson();
		person.setPos(person.getPos().moveX(amount));

		this.background.move(-amount);
	}

	//**

	@Override
	public FrustratedWorldData copy() {
		return this;
	}

	@Override
	public String toString() {
		return "{ frustrated-world-data }";
	}
}