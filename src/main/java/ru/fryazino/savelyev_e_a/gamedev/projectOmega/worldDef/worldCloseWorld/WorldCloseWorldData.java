package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.worldCloseWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.inWorld.WorldData;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;

public final class WorldCloseWorldData<WD extends WorldData> implements CSD<WorldCloseWorldData<WD>> {
	private final WD   worldData;

	public WorldCloseWorldData(WD worldData) {
		this.worldData  = worldData;
	}

	public WD getWorldData() {
		return this.worldData;
	}

	@Override
	public WorldCloseWorldData<WD> copy() {
		return new WorldCloseWorldData<>((WD) worldData.copy());
	}

	@Override
	public String toString() {
		return "{ outWorld-data: " + this.worldData.toString() + " }";
	}
}