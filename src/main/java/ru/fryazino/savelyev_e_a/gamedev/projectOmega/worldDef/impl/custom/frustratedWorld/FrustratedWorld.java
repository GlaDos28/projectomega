package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.impl.custom.frustratedWorld;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.Camera2D;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.GraphicsAdapter;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.elements.Image;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.elements.RepeatingImage;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.elements.animatedModel.AnimatedModel;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics.elements.animatedModel.Sprite;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.Sound;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.IPoint;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.Point;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.worldDef.WorldDef;

import java.awt.event.KeyEvent;

public final class FrustratedWorld {
	private static final Image IMG_BACKGROUND      = new Image("main/resources/media/projectOmegaTestBackground.png",    1900, 1000, true, false);
	private static final Image IMG_BACKGROUND_TREE = new Image("main/resources/media/projectOmegaTestTree.png",          0.5f,       true, false);
	private static final Image IMG_GROUND          = new Image("main/resources/media/projectOmegaFrustrationGround.png", 600, 200,   true, false);
	private static final Image IMG_PERSON_1        = new Image("main/resources/media/projectOmegaPerson1.png",           2f,         true, false);
	private static final Image IMG_PERSON_2        = new Image("main/resources/media/projectOmegaPerson2.png",           2f,         true, false);
	private static final Image IMG_PERSON_3        = new Image("main/resources/media/projectOmegaPerson3.png",           2f,         true, false);
	private static final Image IMG_PERSON_4        = new Image("main/resources/media/projectOmegaPerson4.png",           2f,         true, false);
	private static final Image IMG_PERSON_5        = new Image("main/resources/media/projectOmegaPerson5.png",           2f,         true, false);
	private static final Image IMG_PERSON_6        = new Image("main/resources/media/projectOmegaPerson6.png",           2f,         true, false);

	private static final RepeatingImage RIMG_GROUND = new RepeatingImage(IMG_GROUND, true, false);

	private FrustratedWorld() {}

	public static final WorldDef<FrustratedWorldData> FRUSTRATED_WORLD_DEF = new WorldDef<>(
		(data) -> {
			FrustratedWorldInitWorldData frustratedWorldData = (FrustratedWorldInitWorldData) data;
			return new FrustratedWorldData(
				IMG_BACKGROUND,
				IMG_BACKGROUND_TREE
			);
		},
		(data) -> {
			Sprite staySprite = new Sprite(IMG_PERSON_1, Integer.MAX_VALUE - 1);
			staySprite.linkNext(staySprite);

			Sprite runSprite = new Sprite(IMG_PERSON_1, 10);
			runSprite
				.linkNext(new Sprite(IMG_PERSON_2, 20))
				.linkNext(new Sprite(IMG_PERSON_3, 20))
				.linkNext(new Sprite(IMG_PERSON_4, 20))
				.linkNext(new Sprite(IMG_PERSON_5, 20))
				.linkNext(new Sprite(IMG_PERSON_6, 20))
				.linkNext(runSprite);

			Sound.playSound("main/resources/sounds/Staruha_Mha_Papopotnik.wav");

			return new FrustratedWorldPerson(data.getHuman(), new Point(0, 0), WorldGravity.DEFAULT_GRAVITY, new AnimatedModel(0, staySprite, runSprite));
		},
		(data) -> {
			data.processKeyboardKeysStandard(0);

			FrustratedWorldPerson person = data.getPerson();

			if (person.getAnimatedModel().getCurStateNum() != 0 &&
				(!data.getController().getKeyboardController().getState().isKeyPressed(KeyEvent.VK_A)
					&& !data.getController().getKeyboardController().getState().isKeyPressed(KeyEvent.VK_D)
					|| person.getPos().getY() > FrustratedWorldPerson.PERSON_SURFACE_HEIGHT)) {
				person.getAnimatedModel().setState(0);
			}

			person.processTic();
		},
		(data) -> {
			GraphicsAdapter g = data.getGraphicsData().getGraphics();
			FrustratedWorldPerson person = data.getPerson();

			//** clearing the screen

			g.setColor(java.awt.Color.BLACK);
			g.drawRect(0, 0, data.getGraphicsData().getScrWidth(), data.getGraphicsData().getScrHeight());

			//** filling the background

			IPoint centredCameraPoint = new IPoint(0, g.getConfig().getScrHeight() / 2);

			data.getWorldData().getBackground().draw(g, new Camera2D(centredCameraPoint.sumY((int) person.getPos().getY())));

			//** filling the ground

			RIMG_GROUND.draw(g, new Camera2D(centredCameraPoint.sum(person.getPos().getIPoint())), new Object[] { null, null, new IPoint((int) -person.getPos().getX(), 0), new IPoint(data.getGraphicsData().getScrWidth(), data.getGraphicsData().getScrHeight()) });

			//** putting the person

			data.<FrustratedWorldPerson>getPerson().draw(
				g,
				new Camera2D(centredCameraPoint),
				new IPoint(0, FrustratedWorldPerson.PERSON_SURFACE_HEIGHT),
				person.isLookingLeft(),
				false);
		},
		(data) -> {},
		(data) -> {},
		(data) -> {},
		(data) -> {}
	);
}