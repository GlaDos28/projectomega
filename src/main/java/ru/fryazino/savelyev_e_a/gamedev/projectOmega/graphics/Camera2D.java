package main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.graphics;

import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.interfaces.CSD;
import main.java.ru.fryazino.savelyev_e_a.gamedev.projectOmega.utils.geometry.IPoint;

public final class Camera2D implements CSD<Camera2D> {
	private final IPoint pos;

	public Camera2D(IPoint pos) {
		this.pos = pos;
	}

	//**

	public IPoint getPos() {
		return this.pos;
	}

	//**

	@Override
	public Camera2D copy() {
		return this; /* immutable object */
	}

	@Override
	public String toString() {
		return "{ pos: " + this.pos.toString() + " }";
	}
}